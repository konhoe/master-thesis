#!/bin/bash

RESULTS="./results/"

input=$1
outputType=$2
version=$3
filename="${1%.*}"
filePath="$RESULTS$filename/"
mkdir "$filePath"

if [ "$version" = "5" ]; then 
  echo Version 0.5 - saving to "$filePath"
  ./IfcConvert_v5.exe  --use-element-guids "$input" "$filePath$filename.$outputType"
  ./IfcConvert_v5.exe "$input" "$filePath$filename.xml"
elif [ "$version" = "6" ]; then 
  echo Version 0.6 - saving to "$filePath"
  ./IfcConvert_v6.exe --use-element-hierarchy --use-element-guids "$input" "$filePath$filename.$outputType"
  ./IfcConvert_v6.exe "$input" "$filePath$filename.xml"
fi