﻿using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.IO;
using UnityEngine;

/// <summary>
/// Static store for a configuration loaded from a JSON in Streaming Assets upon startup.
/// </summary>
[Serializable]
public static class ConfigLoader {
    public static Config config = new Config();

    private static string configFile = "config.json";
    static ConfigLoader()
    {
        var p = Path.Combine(Application.streamingAssetsPath, configFile);
        if (!File.Exists(p)) return;

        var text = File.ReadAllText(p);
        config = JsonConvert.DeserializeObject<Config>(text);
    }
}

public class Config
{
    /// <summary>
    /// Path to the ifcConvert executable (http://ifcopenshell.org/ifcconvert)
    /// </summary>
    public string ifcConvertPath;

    /// <summary>
    /// Path to the results of ifcConvert
    /// </summary>
    public string ifcConvertOutputPath;

    public string defaultFtpUrl;
    public string defaultFtpFolder;
    public string defaultFtpPassword;
    public string defaultFtpUsername;

    public Config(){
        ifcConvertPath = "./00-Modelle/IfcConvert_v6.exe";
        ifcConvertOutputPath = "./ConversionResults";
    }
}