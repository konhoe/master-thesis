﻿using FluentFTP;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using UnityEngine;

/// <summary>
/// Handels the connection to the FTP Server, downloads, uploads and storage of local as well as remote projects.
/// </summary>
public class FTPRepository
{
    private const string localDownloadFolder = "ifcDownload";
    private static FTPRepository _singleton = new FTPRepository();
    
    FtpClient client = new FtpClient(
        ConfigLoader.config.defaultFtpUrl, 
        new NetworkCredential(ConfigLoader.config.defaultFtpUsername, ConfigLoader.config.defaultFtpPassword)
    );
    private string storage;
    private string workingDir = ConfigLoader.config.defaultFtpFolder;

    public static FTPRepository GetFTPRepository()
    {
        return _singleton;
    }

    private FTPRepository()
    {
        storage = Path.Combine(Application.persistentDataPath, localDownloadFolder);
        if (!Directory.Exists(storage))
            Directory.CreateDirectory(storage);
    }

    public void SetCredentials(string host, string folder, string username, string password)
    {
        client = new FtpClient(
            host,
            new NetworkCredential(username, password)
        );
        workingDir = folder;
    }

    /// <summary>
    /// Tries to connect to server with the current credentials.
    /// </summary>
    /// <returns></returns>
    public bool TestConnection()
    {
        try
        {
            client.Connect();
            client.SetWorkingDirectory(workingDir);
            client.Disconnect();
            return true;
        }catch (FtpException)
        {
            return false;
        }
    }

    public IEnumerable<string> GetDirectoryNames()
    {
        client.Connect();
        client.SetWorkingDirectory(workingDir);
        var directories = client.GetListing().Select(item => item.Name);
        client.Disconnect();

        return directories;
    }

    public IEnumerable<FtpListItem> GetDirectoryItems()
    {
        client.Connect();
        client.SetWorkingDirectory(workingDir);
        var directories = client.GetListing().Select(item => item);
        client.Disconnect();

        return directories;
    }

    public Task UploadProjectAsync(string project, List<string> files, Action<double> progressCallback)
    {
        client.Connect();
        client.SetWorkingDirectory(workingDir);
        if (client.DirectoryExists(project))
            client.DeleteDirectory(project);
        client.CreateDirectory(project);
        var fileProgress = Enumerable.Repeat(0.0, files.Count).ToList();
        var progressTracking = new Progress<FtpProgress>((x) => {
            fileProgress[x.FileIndex] = x.Progress;
            var totalProgress = fileProgress.Sum() / files.Count;
            Debug.Log($"Upload progress {x.FileIndex} of {files.Count}.");
            Debug.Log(totalProgress);
            progressCallback.Invoke(totalProgress); 
        });
        return client.UploadFilesAsync(files, project, progress:progressTracking).ContinueWith(delegate { client.Disconnect(); });
    }

    public void DeleteRemoteProject(string project)
    {
        client.Connect();
        client.SetWorkingDirectory(workingDir);
        client.DeleteDirectory(project);
        client.Disconnect();
    }

    public string DownloadProject(string project)
    {
        client.Connect();
        client.SetWorkingDirectory(workingDir);
        var files = client.GetNameListing(project);
        var path = Path.Combine(storage, project);

        if (!Directory.Exists(path))
        {
            Directory.CreateDirectory(path);
        }
        else
        {
            Directory.Delete(path, true);
            Directory.CreateDirectory(path);
        }

        client.DownloadFiles(path, files);
        client.Disconnect();

        return path;
    }
    public IEnumerable<string> ListLocalProjects()
    {
        return Directory.GetDirectories(storage).Select(fullPath => Path.GetFileName(fullPath));
    }

    internal void DeleteLocalProject(string project)
    {
        var path = Path.Combine(storage, project);
        Directory.Delete(path, true);
    }

    public string GetLocalPathOfProject(string project)
    {
        return Path.Combine(storage, project);
    }
}
