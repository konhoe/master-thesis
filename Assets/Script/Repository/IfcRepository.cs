﻿using IfcEditorTypes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;
using UnityEngine;

/// <summary>
/// Loads geometry and metadata from ifc files.
/// Geometry is extracted by the Trilib Library, Metadata is parsed
/// from an XML thats created by the ifcConvert (http://ifcopenshell.org/ifcconvert) binary.
/// XML file contains a (scene-) decomposition that contains every IFC Element in the file with its
/// Metadata Attributes, which are stored as references to the other sections in the XML 
/// (materials, properties, types, layers, ...)
/// 
/// For now we are only interested in the following Attributes of an IFC Element:
/// IfcPropertySet - Properties like FireRating, LoadBearing, Density, ...
/// IfcElementQuantity - Specific Quantities like height, width, weight...
/// (Wall/Slab/Outlet/*) Type - Each IFC Element can have its own type eg. IfcWallType, IfcSlabType, IfcOutletType, ...
/// IfcMaterial - Material of this IFC Element eg Wood, Metal, ...
/// 
/// eg:
/// <decomposition>
/// ...
/// <IfcFlowSegment id="1DSquPhGzAGPNp0BhwIyCF" Name="02.0002.02"...>   //the IFC Element with its unique ID
///		<IfcPropertySet xlink:href="#27tm1V9K1DIPWOgNA3uOzc"/>  //A referenced propertySet which can be found in <properties></properties>
///		<IfcMaterial xlink:href="#IfcMaterial_12802"/>  //A referenced material which can be found in <materials></materials>
///	</IfcFlowSegment>
///	...
/// </decomposition>
/// 
/// </summary>
public class IfcRepository
{

    private static readonly IfcRepository _singleton = new IfcRepository();
    private TriLibLoader gameObjectLoader;
    private XmlLoader xmlLoader;

    private List<GameObject> loadedGameObjects = new List<GameObject>();
    public List<XDocument> loadedXmls = new List<XDocument>();

    public List<string> filteredProperties = new List<string>();
    public Dictionary<string, IfcElement> editedIfcElements = new Dictionary<string, IfcElement>();
    private const int ID_LENGTH = 22;

    /// <summary>
    /// Get the singelton instance of the IFC Repository.
    /// </summary>
    /// <returns>A reference to the singleton IfcRepository</returns>
    public static IfcRepository GetIfcRepository()
    {
        return _singleton;
    }

    private IfcRepository()
    {
        this.gameObjectLoader = new TriLibLoader();
        this.xmlLoader = new XmlLoader();
    }

    /// <summary>
    /// Initializes this repository with the given IFC file. Creates a new Gameobject under root, generates & loads an XML from the IFC file.
    /// </summary>
    /// <param name="path">Path to the ifc</param>
    /// <param name="root">Root object to the loaded Object</param>
    /// <returns></returns>
    public GameObject LoadIfcFromPath(string path, GameObject root)
    {
        var loadedGameObject = gameObjectLoader.LoadModel(path, root);
        loadedGameObjects.Add(loadedGameObject);

        var xml = xmlLoader.LoadXmlFromIfc(path);
        loadedXmls.Add(xml);

        return loadedGameObject;
    }

    /// <summary>
    /// Finds a IFCElement by its unique 22 character ID or returns null.
    /// </summary>
    /// <param name="id">Unique ID to search</param>
    /// <returns>Found IFCElement or null.</returns>
    public IfcElement FindByID(string id)
    {
        IfcElement toReturn = null;
        if (editedIfcElements.ContainsKey(id))
        {
            toReturn =  editedIfcElements[id];
        }
        else
        {
            foreach (var xml in loadedXmls)
            {
                var xElement = IfcXmlConverter.FindXmlElementWithID(xml, id);
                if (xElement == null) continue;
                toReturn = IfcXmlConverter.XmlElementToIfcElement(xElement);
                break;
            }
        }
        if (toReturn == null) return toReturn;

        //hide globally filtered Properties
        toReturn.properties.ForEach(prop => {
            prop.isActiveGlobal = !filteredProperties.Contains(prop.name);
        });
        return toReturn;
    }

    /// <summary>
    /// Extracts ID from Gameobject name and finds the corresponding IFCElement.
    /// </summary>
    /// <param name="g">Gameobject which represents the IFCElement.</param>
    /// <returns>The found IFCElement or null if not found or no ID in Gameobject name.</returns>
    internal IfcElement GetIfcElementOfGameObject(GameObject g)
    {
        var id = GetIdFromGameObjectName(g.name);
        if (id == null) return null;
        return FindByID(id);
    }

    /// <summary>
    /// Finds an XML Element with the given ID.
    /// </summary>
    /// <param name="id">ID to look up.</param>
    /// <returns>The found XML Element or null.</returns>
    public XElement XElementFromID(string id)
    {
        foreach (var xml in loadedXmls)
        {
            var elem = IfcXmlConverter.FindXmlElementWithID(xml, id);
            if (elem == null) continue;
            return elem;
        }
        return null;
    }
    /// <summary>
    /// Generates an Export from the loaded IFC files.
    /// </summary>
    /// <param name="loaded">List of loaded (root) GameObjects. (Multiple IFC files)</param>
    /// <returns>A IFC Export with filled ifcElements</returns>
    internal IfcExport XmlToIfcJsonCollection(List<GameObject> loaded)
    {
        var collection = new IfcExport();
        foreach (GameObject g in loaded)
        {
            var gameObjects = g.GetComponentsInChildren<Transform>();
            foreach (var child in gameObjects)
            {
                var id = GetIdFromGameObjectName(child.name);
                if (id == null) continue;

                var ifcElement = FindByID(id);
                collection.ifcElements.Add(ifcElement);
            }
        }

        return collection;
    }

    /// <summary>
    /// Extracts the ID from a GameObject Name
    /// </summary>
    /// <param name="name">The name of a loaded Gameobject</param>
    /// <returns>The ID of a Gameobject/IFCElement or null.</returns>
    public string GetIdFromGameObjectName(string name)
    {
        if (name.Length <= ID_LENGTH) return null;
        if (name[name.Length - ID_LENGTH - 1] != '_') return null;

        return name.Substring(name.Length - ID_LENGTH);
    }

    /// <summary>
    /// Checks wether the XML Element is a reference to another XML Element.
    /// </summary>
    /// <param name="x">XML Element to check.</param>
    /// <returns>Wether x is a reference.</returns>
    public bool IsReference(XElement x)
    {
        return x.Attribute("{http://www.w3.org/1999/xlink}href") != null;
    }

    /// <summary>
    /// Saves a change to an IFC Element. Eg when its assigned to a new layer, its properties changed or a comment is added.
    /// </summary>
    /// <param name="updated">The changed IFC Element.</param>
    public void UpdateIfcElement(IfcElement updated)
    {
        editedIfcElements[updated.id] = updated;
    }

    /// <summary>
    ///  Returns a list of all Properties of all loaded IFC files.
    /// </summary>
    /// <returns>Distinct list of all properties of all loaded IFC files</returns>
    public List<string> GetPropertyList()
    {
        List<string> properties = new List<string>();
        foreach(var xml in loadedXmls)
        {
            properties.AddRange(IfcXmlConverter.GetDistinctProperties(xml));
        }

        return properties.Distinct().ToList();
    }

    /// <summary>
    /// Sets the Global Property Filter value of the given property.
    /// </summary>
    /// <param name="shouldShow">Wether the given property should be marked visible in the Export.</param>
    /// <param name="property">The property to change.</param>
    public void SetPropertyFilter(bool shouldShow, string property)
    {
        if (shouldShow)
            filteredProperties.Remove(property);
        else
            filteredProperties.Add(property);
    }

  internal void ResetRepository()
  {
      loadedGameObjects = new List<GameObject>();
      loadedXmls = new List<XDocument>();

      filteredProperties = new List<string>();
      editedIfcElements = new Dictionary<string, IfcElement>();
  }
}

