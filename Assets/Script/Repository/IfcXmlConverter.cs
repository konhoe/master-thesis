﻿using System.Collections;
using System.Collections.Generic;
using System.Xml;
using System.Xml.Linq;
using System.Linq;
using UnityEngine;
using System;
using IfcEditorTypes;

/// <summary>
/// Helper class for the IFC Repository, deals with parsing the IFC XML files.
/// </summary>
public static class IfcXmlConverter
{
    /// <summary>
    /// Constructs an IFC Element from a XML Element.
    /// </summary>
    /// <param name="elem">XML Element to convert.</param>
    /// <returns>A new IFC Element with all properties and fields of the XML Element.</returns>
    public static IfcElement XmlElementToIfcElement(XElement elem)
    {
        IfcElement ifcElement = new IfcElement();
        ifcElement.id = elem.Attribute("id").Value;

        if (elem.Attribute("Name") != null)
            ifcElement.elementName = elem.Attribute("Name").Value;
        else
            ifcElement.elementName = elem.Name.LocalName;

        // Get references to some important properties of this IFC Element
        var xLinked = new List<XElement>();
        xLinked.AddRange(elem.Elements("IfcPropertySet"));
        xLinked.AddRange(elem.Elements("IfcElementQuantity"));
        xLinked.AddRange(elem.Elements(elem.Name + "Type"));
        xLinked.AddRange(elem.Elements("IfcMaterial"));

        var ifcProperties = new List<IfcProperty>();
        // Map the references to the properties via the linked ID
        foreach (var x in xLinked)
        {
            var linkId = x.Attribute("{http://www.w3.org/1999/xlink}href").Value.Substring(1); //Remove leading #
            var linkedElement = FindXmlElementWithID(elem.Document, linkId);

            IfcProperty prop = XmlElementToIfcProperty(linkedElement);
            if (!ifcProperties.Exists(p => p.id == prop.id)) // Avoid Duplicate Properties, IFC to XML Converter tends to add Properties multiple times
                ifcProperties.Add(prop);
        }
        ifcElement.properties = ifcProperties;
        return ifcElement;
    }

    /// <summary>
    /// Looks for an XML Element by ID in a given XML Document.
    /// </summary>
    /// <param name="source">The XML Document to filter.</param>
    /// <param name="linkId">The ID to look for</param>
    /// <returns>A XML Element with ID or null.</returns>
    public static XElement FindXmlElementWithID(XDocument source, string linkId)
    {
        XElement found = (from el in source.Descendants()
                         where el.Attribute("id")?.Value == linkId
                         select el).FirstOrDefault();
        return found;
    }

    /// <summary>
    /// BUilds a IFC Property from a XML Element.
    /// If property has 2 attributes its assumed to be a key-value pair.
    /// 
    /// </summary>
    /// <param name="elem">Xelement to convert into an IFC Property</param>
    /// <returns>A new IfcProperty with a filled attributes list.</returns>
    private static IfcProperty XmlElementToIfcProperty(XElement elem)
    {
        var toReturn = new IfcProperty();
        toReturn.id = elem.Attribute("id")?.Value;
        toReturn.name = elem.Attribute("Name")?.Value;

        foreach (var xElement in elem.Descendants())
        {
            var attributes = xElement.Attributes().ToList();

            if (attributes.Count == 2)
            {
                // Assume that property is a key value pair
                toReturn.attributes.Add($"{attributes[0].Value}: {attributes[1].Value}");
            }
            else
            {
                attributes.ForEach(attribute => toReturn.attributes.Add($"{attribute.Name.LocalName}: {attribute.Value}"));
            }
        }

        return toReturn;
    }

    /// <summary>
    /// Generates a list with all IFC Properties, Quantities, Types and Materials with unique names from a XML Document.
    /// </summary>
    /// <param name="xDocument">XML Document to filter.</param>
    /// <returns>A list with all property names.</returns>
    public static IEnumerable<string> GetDistinctProperties(XDocument xDocument)
    {
        var properties = xDocument.Descendants("properties").Elements();
        var quantities = xDocument.Descendants("quantities").Elements();
        var types = xDocument.Descendants("types").Elements();
        var materials = xDocument.Descendants("materials").Elements();
        
        var allProps = properties.Concat(quantities).Concat(types).Concat(materials);

        var toReturn = allProps.GroupBy(prop => prop.Attribute("Name")?.Value).Select(elem => elem.Key);

        return toReturn; 
    }

}
