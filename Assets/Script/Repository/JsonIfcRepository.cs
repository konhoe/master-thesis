﻿using IfcEditorTypes;
using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

/// <summary>
/// Handles loading data from an existing/downloaded Project
/// </summary>
public class JsonIfcRepository
{
    private static readonly JsonIfcRepository _singleton = new JsonIfcRepository();

    private IfcExport ifcJsonCollection;

    public static JsonIfcRepository GetJsonIfcRepository()
    {
        return _singleton;
    }

    private JsonIfcRepository(){}

    /// <summary>
    /// Loads the provided JSON and initializes the repository with the deserialized values.
    /// </summary>
    /// <param name="pathToJson">Full path to the JSON file.</param>
    public void Initialize(string pathToJson)
    {
        var json = System.IO.File.ReadAllText(pathToJson);
        var serializerSettings = new JsonSerializerSettings();

        ifcJsonCollection = JsonConvert.DeserializeObject<IfcExport>(json, serializerSettings);
    }

    public Vector3 GetPosition(string ifcFile)
    {
        IfcTransform transform = ifcJsonCollection.transforms.SingleOrDefault(t => t.target == ifcFile);
        if(transform.target == null)
            return Vector3.zero;
        else
            return transform.position;
    }

    public Quaternion GetRotation(string ifcFile)
    {
        IfcTransform transform = ifcJsonCollection.transforms.SingleOrDefault(t => t.target == ifcFile);
        if (transform.target == null)
            return Quaternion.identity;
        else
            return Quaternion.Euler(transform.rotation);
    }

    public Vector3 GetScale(string ifcFile)
    {
        IfcTransform transform = ifcJsonCollection.transforms.SingleOrDefault(t => t.target == ifcFile);
        if (transform.target == null)
            return Vector3.one;
        else
            return transform.scale;
    }

    internal IfcElement GetElementById(string id)
    {
        return ifcJsonCollection.ifcElements.Find(elem => elem.id == id);
    }

    public IEnumerable<Layer> GetAllLayers()
    {
        HashSet<Layer> toReturn = new HashSet<Layer>();
        foreach(var elem in ifcJsonCollection.ifcElements)
        {
            foreach(var layer in elem.layers)
            {
                toReturn.Add(layer);
            }
        }
        return toReturn;
    }

    public IEnumerable<string> GetAllButtons()
    {
        HashSet<string> toReturn = new HashSet<string>();
        foreach (var elem in GetAllLayers())
        {
            toReturn.Union(elem.groups);
        }
        return toReturn;
    }
    public List<Annotation> GetAllAnnotations()
    {
        return ifcJsonCollection.annotations;
    }

    public IEnumerable<string> GetElementIdsOnLayer(string layer)
    {
        return ifcJsonCollection.ifcElements
            .FindAll(ifc => ifc.layers.FindAll(l => l.name == layer).Count > 0)
            .Select(elem => elem.id);
    }


}
