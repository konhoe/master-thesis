﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using TriLib;

/// <summary>
/// We use the Unity Asset TriLib to load the geometry representation of a IFC file.
/// As this is a commercial library a licence needs to be aquired from the Unity Assetstore.
/// TODO: Replace Trilib with a open & free alternative (eg https://github.com/intelligide/assimp-unity or https://github.com/assimp/assimp-net)
/// </summary>
public class TriLibLoader  {

    /// <summary>
    /// Loads the given IFC file and turns it into a Gameobject.
    /// We also apply a double sided Material to all loaded Gameobjects to account for inconsistent normals and adjust the default Glossines.
    /// </summary>
    /// <param name="path">Path to the IFC File.</param>
    /// <param name="parent">Parent Gameobject of the new Gameobject.</param>
    /// <returns></returns>
    public GameObject LoadModel(string path, GameObject parent)
    {
        var assetLoader = new AssetLoader();
        var assetLoaderOptions = AssetLoaderOptions.CreateInstance();

        //Global Scale not working - Applying scale in LoadManager
        //assetLoaderOptions.AdvancedConfigs.Add(AssetAdvancedConfig.CreateConfig(AssetAdvancedPropertyClassNames.GlobalScaleFactor, 0.0001f));

        // List of Postprocessing Steps that yield a good result after trial & error. Not universally the best result.
        assetLoaderOptions.PostProcessSteps =
            AssimpPostProcessSteps.CalcTangentSpace
            | AssimpPostProcessSteps.GenSmoothNormals
            | AssimpPostProcessSteps.ImproveCacheLocality
            | AssimpPostProcessSteps.LimitBoneWeights
            | AssimpPostProcessSteps.Triangulate
            | AssimpPostProcessSteps.GenUvCoords
            | AssimpPostProcessSteps.SortByPType
            | AssimpPostProcessSteps.FindInvalidData
            | AssimpPostProcessSteps.MakeLeftHanded
            | AssimpPostProcessSteps.FlipWindingOrder;

        assetLoaderOptions.RotationAngles = new Vector3(90, 0, 0);
        assetLoaderOptions.GenerateMeshColliders = true; //Needed for Raycast Selection of Gameobjects.
        GameObject loadedObject = assetLoader.LoadFromFileWithTextures(path, assetLoaderOptions, parent);

        // Apply doubleSided material to all loaded objects.
        Shader doubleSided = Shader.Find("Standard-DoubleSided");
        foreach (var r in loadedObject.GetComponentsInChildren<Renderer>())
        {
            r.material.shader = doubleSided;
            if (r.material.HasProperty("_Glossiness")) r.material.SetFloat("_Glossiness", 0.2f); //Most Models were far too glossy.
        }
        return loadedObject;
    }
}
