﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Xml.Linq;
using UnityEngine;

/// <summary>
/// Handels Loading of IFC XML Files from IFC files through the IFC Convert binary (http://ifcopenshell.org/ifcconvert)
/// </summary>
public class XmlLoader  {

    private string ifcConvertPath;
    private string outputPath;

    public XmlLoader()
    {
        this.ifcConvertPath = ConfigLoader.config.ifcConvertPath;
        this.outputPath = ConfigLoader.config.ifcConvertOutputPath;
    }

    /// <summary>
    /// Launch ifcconvert.exe at ifcConvertPath and output xml to ./{outputPath}/{ModelName}/.
    /// Deletes existing XML file before starting a conversion.
    /// 
    /// </summary>
    /// <param name="ifcPath">Path to Input IFC File.</param>
    /// <returns>The nongeometric information of the IFC File as a XDocument.</returns>
    public XDocument LoadXmlFromIfc(string ifcPath)
    {
        Directory.CreateDirectory(outputPath);
        ifcConvertPath = Path.Combine(Directory.GetCurrentDirectory(), ifcConvertPath);
        string fileName = Path.GetFileNameWithoutExtension(ifcPath);
        string outputFolder = Directory.CreateDirectory(Path.Combine(outputPath, fileName)).FullName;
        string outputFile = Path.Combine(outputFolder, fileName + ".xml");
        if (File.Exists(outputFile)) File.Delete(outputFile);

        //https://stackoverflow.com/questions/4291912/process-start-how-to-get-the-output
        System.Diagnostics.Process process = new System.Diagnostics.Process();
        process.StartInfo.FileName = ifcConvertPath;
        process.StartInfo.Arguments = "\""+ifcPath + "\"" + " " + "\"" +outputFile+ "\"";
        process.StartInfo.UseShellExecute = false;
        process.StartInfo.RedirectStandardOutput = true;
        process.StartInfo.RedirectStandardError = true;
        process.StartInfo.RedirectStandardInput = true;
        process.StartInfo.CreateNoWindow = true;
        process.OutputDataReceived += new System.Diagnostics.DataReceivedEventHandler(OutputHandler);
        process.ErrorDataReceived += new System.Diagnostics.DataReceivedEventHandler(OutputHandler);
        process.Start();
        process.BeginOutputReadLine();
        process.BeginErrorReadLine();
        process.WaitForExit();

        XDocument xmlFile = XDocument.Load(outputFile);

        return xmlFile;
    }

    private void OutputHandler(object sendingProcess, System.Diagnostics.DataReceivedEventArgs outLine)
    {
        return; // IfcConvert doesn't Ouput in a way that we can use here. Unity single thread probably interferes with logging output.
    }

}
