﻿using IfcEditorTypes;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Wraps Annotaion in a MonoBehaviour to allow attaching it to a Gameobject.
/// </summary>
public class AnnotationComponent : MonoBehaviour
{
    public Annotation annotation;
}
