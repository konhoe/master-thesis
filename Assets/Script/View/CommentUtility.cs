﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Manages the Comment Text Input for a selected IFC Element. Loads comment on selectionChange and saves the comment in the IfcRepository after editing. 
/// </summary>
public class CommentUtility : MonoBehaviour
{
    [SerializeField]
    private TMPro.TMP_InputField input;

    private SelectTool selectTool;
    private IfcRepository ifcRepository;

    void Awake()
    {
        ifcRepository = IfcRepository.GetIfcRepository();
        selectTool = FindObjectOfType<SelectTool>();
        selectTool.OnSelectionChange.AddListener(delegate
        {
            var selection = selectTool.CurrentSelection;
            if (selection == null) return;

            var ifc = ifcRepository.GetIfcElementOfGameObject(selection);
            input.text = ifc.comment;
        });

        input.onEndEdit.AddListener(delegate
        {
            var selection = selectTool.CurrentSelection;
            if (selection == null) return;

            var ifc = ifcRepository.GetIfcElementOfGameObject(selection);
            ifc.comment = input.text;
            ifcRepository.UpdateIfcElement(ifc);
        });
    }



}
