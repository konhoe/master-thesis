﻿using System.Collections;
using System.Linq;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using System;

/// <summary>
/// Manages the Download UI. Calls FTP Repository to retrieve Projects on Server and can load Projects via the LoaderManager.
/// </summary>
public class DownloadManager : MonoBehaviour
{
    [SerializeField]
    private Transform downloadWindow;

    [SerializeField]
    private Transform downloadButtonHolder;

    [SerializeField]
    private GameObject downloadButtonPrefab;

    private FTPRepository ftp;
    private LoaderManager loader;
    private FtpCredentialsManager ftpCredentialsManager;

    void Start()
    {
        ftp = FTPRepository.GetFTPRepository();
        loader = FindObjectOfType<LoaderManager>();
        ftpCredentialsManager = FindObjectOfType<FtpCredentialsManager>();
        Refresh();

        ftpCredentialsManager.OnCredentialsChange.AddListener(Refresh);
    }

    public void Refresh()
    {
        foreach (Transform t in downloadButtonHolder)
            Destroy(t.gameObject);

        var projectsWithDate = ftp.GetDirectoryItems().OrderByDescending(dir => dir.Modified);
        if (projectsWithDate.Count() == 0) return;
        var remoteProjects = projectsWithDate.Select( i => i.Name);
        var localProjects = ftp.ListLocalProjects().OrderByDescending(name => {
            var matchingServerProjects = projectsWithDate.Where(dirWithDate => dirWithDate.Name == name);
            if (matchingServerProjects.Count() > 0)
                return matchingServerProjects.First().Modified;
            else
                return new DateTime();
        });

        foreach(var projectName in localProjects)
        {
            var projectDate = projectsWithDate.FirstOrDefault(i => i.Name == projectName)?.Modified.ToString("dd.MM.yyyy");
            var local = Instantiate(downloadButtonPrefab, downloadButtonHolder);
            var uiText = local.transform.GetChild(0).GetComponent<TextMeshProUGUI>();
            uiText.text = projectName + (projectDate != null ? $" - <i>{projectDate}</i>" : "");

            var openButton = local.GetComponent<Button>();
            openButton.onClick.AddListener(delegate
            {
                loader.LoadProject(ftp.GetLocalPathOfProject(projectName));
                downloadWindow.gameObject.SetActive(false);
            });

            var deleteBtn = local.transform.Find("Delete").GetComponent<Button>();
            deleteBtn.onClick.AddListener(delegate {
                ftp.DeleteLocalProject(projectName);
                Refresh();
            });
        }

        var onlyRemoteProjects = remoteProjects.Except(localProjects);

        foreach(var projectName in onlyRemoteProjects)
        {
            var projectDate = projectsWithDate.First(i => i.Name == projectName)?.Modified.ToString("dd.MM.yyyy");

            var remote = Instantiate(downloadButtonPrefab, downloadButtonHolder);
            var uiText = remote.transform.GetChild(0).GetComponent<TextMeshProUGUI>();
            uiText.text = $"{projectName} - <i>{projectDate}</i>";
            uiText.color = Color.gray;

            var downloadBtn = remote.GetComponent<Button>();
            downloadBtn.onClick.AddListener(delegate
            {
                StartCoroutine(DownloadProject(uiText, projectName));
            });
            remote.transform.Find("Delete").gameObject.SetActive(false);
        }
    }

    private IEnumerator DownloadProject(TextMeshProUGUI text, string p)
    {
        text.text = "Downloading...";
        yield return 0;
        ftp.DownloadProject(p);
        Refresh();
    }
}
