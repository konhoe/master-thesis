﻿using FluentFTP.Proxy;
using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.Events;

/// <summary>
/// Sets the FTP Connection Input fields and handles their changes.
/// </summary>
public class FtpCredentialsManager : MonoBehaviour
{
    public UnityEvent OnCredentialsChange;

    [SerializeField]
    private List<TMP_InputField> hostInputs, folderInputs, usernameInputs, passwordInputs;
    [SerializeField]
    private TMP_Text testConnectionText;

    private string host, folder, username, password;
    private FTPRepository ftpRepository;

    private void Awake()
    {
        ftpRepository = FTPRepository.GetFTPRepository();
                
        hostInputs.ForEach(i => i.onEndEdit.AddListener(text =>
        {
            host = text;
            SyncInputs(hostInputs, text);
            ftpRepository.SetCredentials(host, folder, username, password);
            OnCredentialsChange.Invoke();
        }));
        folderInputs.ForEach(i => i.onEndEdit.AddListener(text =>
        {
            folder = text;
            SyncInputs(folderInputs, text);
            ftpRepository.SetCredentials(host, folder, username, password);
            OnCredentialsChange.Invoke();
        }));
        usernameInputs.ForEach(i => i.onEndEdit.AddListener(text =>
        {
            username = text;
            SyncInputs(usernameInputs, text);
            ftpRepository.SetCredentials(host, folder, username, password);
            OnCredentialsChange.Invoke();
        }));
        passwordInputs.ForEach(i => i.onEndEdit.AddListener(text =>
        {
            password = text;
            SyncInputs(passwordInputs, text);
            ftpRepository.SetCredentials(host, folder, username, password);
            OnCredentialsChange.Invoke();
        }));

        Reset();
    }

    /// <summary>
    /// Resets the Credentials to the default values from the config.json.
    /// </summary>
    public void Reset()
    {
        host = ConfigLoader.config.defaultFtpUrl;
        folder = ConfigLoader.config.defaultFtpFolder;
        username = ConfigLoader.config.defaultFtpUsername;
        password = ConfigLoader.config.defaultFtpPassword;

        SyncInputs(hostInputs, host);
        SyncInputs(folderInputs, folder);
        SyncInputs(usernameInputs, username);
        SyncInputs(passwordInputs, password);
        ftpRepository.SetCredentials(host, folder, username, password);
    }

    public void TestConnection()
    {
        bool isWorking = ftpRepository.TestConnection();

        if (isWorking)
            StartCoroutine(SetTextForSeconds(testConnectionText, "<color=#013220>Connected!</color>", 1.0f));
        else
            StartCoroutine(SetTextForSeconds(testConnectionText, "<color=#8b0000>No Connection!</color>", 1.0f));

    }

    private IEnumerator SetTextForSeconds(TMP_Text target, string text, float seconds)
    {
        string oldText = target.text;
        target.text = text;
        yield return new WaitForSeconds(seconds);
        target.text = oldText;
    }

    private void SyncInputs(List<TMP_InputField> inputs, string text)
    {
        inputs.ForEach(i => i.SetTextWithoutNotify(text));
        Canvas.ForceUpdateCanvases();
    }

}
