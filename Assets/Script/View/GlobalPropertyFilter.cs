﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// UI Monobehaviour that handles changes to the Global Property Filter.
/// </summary>
public class GlobalPropertyFilter : MonoBehaviour
{
    [SerializeField]
    private Transform propertiesHolder;
    
    [SerializeField]
    private GameObject propertyPrefab;

    private IfcRepository ifcRepository;
    private List<string> propertyNames = new List<string>();

    // Start is called before the first frame update
    void Awake()
    {
        FindObjectOfType<LoaderManager>().objectLoadedEvent.AddListener(() => Initialize());
    }

    private void Initialize()
    {
        var height = propertyPrefab.GetComponent<RectTransform>().sizeDelta.y + 10;

        ifcRepository = IfcRepository.GetIfcRepository();
        propertyNames = ifcRepository.GetPropertyList();
        propertyNames.ForEach(prop =>
        {
            var propObject = Instantiate(propertyPrefab, propertiesHolder);
            var propText = propObject.GetComponentInChildren<TMPro.TextMeshProUGUI>();
            
            var toggle = propObject.GetComponentInChildren<Toggle>();
            toggle.SetIsOnWithoutNotify(!ifcRepository.filteredProperties.Contains(prop));
            toggle.onValueChanged.AddListener((isOn) => SavePropertySetting(isOn, prop));
            
            propText.text = prop;
        });

        var holderRect = propertiesHolder.GetComponent<RectTransform>();
        holderRect.sizeDelta = new Vector2(holderRect.sizeDelta.x, height * propertyNames.Count + 10);
        Canvas.ForceUpdateCanvases();
    }

    public void SavePropertySetting(bool shouldShow, string propertyName)
    {
        ifcRepository.SetPropertyFilter(shouldShow, propertyName);
    }
}
