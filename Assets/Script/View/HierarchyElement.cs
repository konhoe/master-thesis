﻿using System.Collections;
using System.Collections.Generic;
using System.Xml.Linq;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// An Element in the HierarchyView, represents an IFC Element by its ID. Can expand/collapse to show/hide children. 
/// </summary>
public class HierarchyElement : MonoBehaviour {
    public string id;
    public Image expandArrow;
    public Image background;

    private HierarchySelector hierarchySelector;
    private HierarchyView hierarchyView;
    private LoaderManager loaderManager;
    private IfcRepository ifcRepository;
    private bool isToplevel;
    private bool hasChildren;

    public bool IsToplevel {
        get => isToplevel;
        set {
            isToplevel = value;
            expandArrow.transform.parent.gameObject.SetActive(true);
            expandArrow.transform.eulerAngles = isToplevel ? new Vector3(0, 0, 180) : Vector3.zero;
        }
    }

    public bool HasChildren {
        get => hasChildren;
        set
        {
            hasChildren = value;
            if (isToplevel) return;
            expandArrow.transform.parent.gameObject.SetActive(hasChildren);
        }
    }

    public void Start()
    {
        hierarchySelector = FindObjectOfType<HierarchySelector>();
        hierarchyView = FindObjectOfType<HierarchyView>();
        loaderManager = FindObjectOfType<LoaderManager>();
        ifcRepository = IfcRepository.GetIfcRepository();
    }

    public void Clicked()
    {
        var referencedGameObject = loaderManager.GetGameObjectById(id);
        if (referencedGameObject == null) return;
        Debug.Log($"New Hierarchy Selection: {referencedGameObject.name}");
        hierarchySelector.Select(referencedGameObject);
    }

    public void Expand()
    {
        hierarchyView.Expand(id);
    }

    public void Delete()
    {
        var elem = ifcRepository.FindByID(id);
        elem.deleted = true;
        ifcRepository.UpdateIfcElement(elem);
        var g = loaderManager.GetGameObjectById(id);
        foreach(var renderer in g.GetComponentsInChildren<Renderer>())
        {
            //Ignore Annotations when hiding Gameobject
            if (renderer.gameObject.layer == LayerMask.GetMask("Annotation")) continue;
            renderer.enabled = false;
            var collider = renderer.GetComponent<Collider>();
            if (collider != null) collider.enabled = false;
        }
        Destroy(this.gameObject);
        //Set HierarchyView to Parent TODO: extract this to somewhere else.
        hierarchyView.Expand(ifcRepository.XElementFromID(id).Parent.Attribute("id").Value);
    }
    
}
