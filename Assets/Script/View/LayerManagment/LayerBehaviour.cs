﻿using IfcEditorTypes;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

/// <summary>
/// Represents a Layer in the Layers Window. Relays changes in the input Fields to the underlying Layer.
/// </summary>
public class LayerBehaviour : MonoBehaviour
{
    private Layer layer = new Layer();
    private LayerManager layerManager;
    private TMPro.TMP_InputField nameInput;
    private MultiSelectDropdown dropdown;

    public Layer Layer
    {
        get => layer;
        set {
            layer = value;
            if (nameInput == null) return;
            nameInput.text = value.name;
            nameInput.textComponent.UpdateMeshPadding();
        }
    }

    public void Delete()
    {
        layerManager.RemoveLayer(this);
        Destroy(gameObject);
        Destroy(this);
    }

    public void Duplicate()
    {
        layerManager.DuplicateLayer(this);
    }

    private void Awake()
    {
        nameInput = GetComponentInChildren<TMPro.TMP_InputField>();
        nameInput.text = layer.name;
        Canvas.ForceUpdateCanvases();
        nameInput.onDeselect.AddListener(OnLayerNameChange);
        layerManager = FindObjectOfType<LayerManager>();
        dropdown = GetComponentInChildren<MultiSelectDropdown>();
        var options = dropdown.Options;
        layer.groups.ForEach(g => options[g] = true);
        dropdown.UpdateDropdown(options);

        dropdown.onValueChanged.AddListener(delegate
        {
            layer.groups = dropdown.Options.Where(o => o.Value).Select(kv => kv.Key).ToList();
        });
    }

    private void OnLayerNameChange(string arg0)
    {
        layer.name = nameInput.text;
        layerManager.UpdateLayerList();
    }
}
