﻿using System;
using System.Collections.Generic;
using System.Linq;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Represents a LayerGroup (LayerButton) in the Layer Group Window. 
/// </summary>
public class LayerGroup : MonoBehaviour
{
    [SerializeField]
    private Button orderUpBtn;
    [SerializeField]
    private Button orderDownBtn;
    [SerializeField]
    private Toggle visibilityToggle;

    private TMP_InputField nameInput;
    private LayerManager layerManager;

    public bool IsVisible { get => visibilityToggle.isOn; }
    public string GroupName { 
        get => nameInput.text;
        set {
            nameInput = GetComponentInChildren<TMP_InputField>();
            nameInput.text = value;
        }
    }

    private void Awake()
    {
        layerManager = FindObjectOfType<LayerManager>();
        nameInput = GetComponentInChildren<TMP_InputField>();
        nameInput.onDeselect.AddListener(OnButtonNameChange);
    }

    private void OnButtonNameChange(string arg0)
    {
        var allGroups = FindObjectsOfType<LayerGroup>();
        Array.ForEach(allGroups, g =>
        {
            if (g == this) return;
            if (g.GroupName == this.GroupName)
                this.nameInput.text = "ERROR - Duplicate Name";
        });
        layerManager.UpdateLayerGroupList();

    }

    public void Delete()
    {
        layerManager.RemoveLayerGroup(this);
        Destroy(this.gameObject);
    }

    public void OrderUp() => layerManager.OrderGroupUp(this);

    public void OrderDown() => layerManager.OrderGroupDown(this);

    public void PreviewVisibility(bool preview)
    {
        layerManager.PreviewGroupVisibility(preview, this.GroupName);
    }

}
