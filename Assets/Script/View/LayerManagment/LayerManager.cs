﻿using IfcEditorTypes;
using System;
using System.Collections.Generic;
using System.Linq;
using TMPro;
using UnityEngine;

/// <summary>
/// Handles refernces to Layers and LayerGroups.
/// </summary>
public class LayerManager : MonoBehaviour
{
    [SerializeField]
    private GameObject layerGroupHolder, layerGroupPrefab, layerHolder, layerPrefab, layerSelectionHolder, layerSelectionPrefab;

    List<LayerGroup> layerGroups = new List<LayerGroup>();
    List<LayerBehaviour> layerBehaviours = new List<LayerBehaviour>();
    private LoaderManager loaderManager;
    private AnnotateTool annotateTool;

    private void Start()
    {
        loaderManager = FindObjectOfType<LoaderManager>();
        annotateTool = FindObjectOfType<AnnotateTool>();
    }

    public void DuplicateLayer(LayerBehaviour layerBehaviour)
    {
        var newLayer = new Layer();
        newLayer.groups = new List<string>(layerBehaviour.Layer.groups);
        newLayer.name = layerBehaviour.Layer.name +" Copy";
        newLayer.children = new HashSet<Transform>(layerBehaviour.Layer.children);

        AddLayer(newLayer);
    }

    public void Initialize(IEnumerable<Layer> layers, IEnumerable<string> groups)
    {
        foreach (string g in groups)
        {
            AddLayerGroup(g);
        }

        foreach (Layer l in layers)
        {
            
            if (!string.IsNullOrEmpty(l.name) && !layerBehaviours.Exists(b => b.Layer == l))
            {
                AddLayer(l);
            }
        }
    }

    private void AddLayerGroup(string group)
    {
        var newGroup = GameObject.Instantiate(layerGroupPrefab, layerGroupHolder.transform).GetComponent<LayerGroup>();
        newGroup.GroupName = group;
        layerGroups.Add(newGroup);
        AddLayerGroupToDropdowns(newGroup);
    }

    public void AddLayerGroup()
    {
        if (layerGroups.Count > 0 && layerGroups.Last().GroupName == "")
            return;
        var newGroup = GameObject.Instantiate(layerGroupPrefab, layerGroupHolder.transform).GetComponent<LayerGroup>();
        layerGroups.Add(newGroup);
        AddLayerGroupToDropdowns(newGroup);
    }

    public void RemoveLayerGroup(LayerGroup group)
    {
        layerGroups.Remove(group);
        RemoveLayerGroupFromDropdowns(group);
    }

    internal void OrderGroupUp(LayerGroup group)
    {
        var currentPosition = layerGroups.IndexOf(group);
        if (currentPosition == 0) return;
        layerGroups.Remove(group);
        layerGroups.Insert(--currentPosition, group);
        group.transform.SetSiblingIndex(currentPosition);
    }

    internal List<Layer> GetLayers()
    {
        return layerBehaviours.Select(behaviour => behaviour.Layer).ToList();
    }

    internal List<string> GetLayerGroups()
    {
        return layerGroups.Select(group => group.GroupName).ToList();
    }

    /// <summary>
    /// preview true: Disables all gameobject except for the group.
    /// preview false: Activates all gameobjects.
    /// </summary>
    /// <param name="preview">Preview State</param>
    /// <param name="groupName">Group to preview</param>
    internal void PreviewGroupVisibility(bool preview, string groupName)
    {
        foreach(var t in loaderManager.objectRoot.GetComponentsInChildren<Transform>(true))
        {
            t.gameObject.SetActive(!preview);
        }
        if (!preview) return;

        var layersInGroup = layerBehaviours.Select(layerBehaviour => layerBehaviour.Layer).Where(layer => layer.groups.Contains(groupName));

        foreach (var layer in layersInGroup)
        {
            foreach (var child in layer.children)
                foreach (var t in child.gameObject.GetComponentsInParent<Transform>(true)) // we have to activate all parents aswell
                    t.gameObject.SetActive(true);
        }
    }

    internal void OrderGroupDown(LayerGroup group)
    {
        var currentPosition = layerGroups.IndexOf(group);
        if (currentPosition == layerGroups.Count - 1) return;
        layerGroups.Remove(group);
        layerGroups.Insert(++currentPosition, group);
        group.transform.SetSiblingIndex(currentPosition);
    }

    public void AddLayer()
    {
        if (layerBehaviours.Count > 0 && layerBehaviours.Last().Layer.name == "")
            return;
        var layerBehaviour = Instantiate(layerPrefab, layerHolder.transform).GetComponent<LayerBehaviour>();
        layerBehaviours.Add(layerBehaviour);
        var layerSelection = Instantiate(layerSelectionPrefab, layerSelectionHolder.transform).GetComponent<LayerSelectionButton>();
        layerSelection.Layer = layerBehaviour.Layer;
        annotateTool.AddLayer(layerBehaviour.Layer);
        UpdateLayerGroupList();
    }

    private void AddLayer(Layer layer)
    {
        var layerBehaviour = Instantiate(layerPrefab, layerHolder.transform).GetComponent<LayerBehaviour>();
        layerBehaviour.Layer = layer;
        layerBehaviours.Add(layerBehaviour);
        var layerSelection = Instantiate(layerSelectionPrefab, layerSelectionHolder.transform).GetComponent<LayerSelectionButton>();
        layerSelection.Layer = layerBehaviour.Layer;
        annotateTool.AddLayer(layerBehaviour.Layer);
        UpdateLayerGroupList();
    }


    public void RemoveLayer(LayerBehaviour layerBehaviour)
    {
        layerBehaviours.Remove(layerBehaviour);
        foreach (Transform selectionButtonTransform in layerSelectionHolder.transform)
        {
            var button = selectionButtonTransform.GetComponent<LayerSelectionButton>();
            if (button != null && button.Layer == layerBehaviour.Layer)
                Destroy(button.gameObject);
        }
        foreach (var elem in IfcRepository.GetIfcRepository().editedIfcElements)
        {
            elem.Value.layers.Remove(layerBehaviour.Layer);
        }
        annotateTool.RemoveLayer(layerBehaviour.Layer);
    }

    internal void UpdateLayerList()
    {
        foreach(Transform child in layerSelectionHolder.transform)
        {
            var layerSelection = child.GetComponent<LayerSelectionButton>();
            layerSelection.RefreshButtonLabel();
        }
        annotateTool.UpdateLayerList();
    }

    internal void UpdateLayerGroupList()
    {
        UpdateLayerGroupDropdowns();
    }

    private void AddLayerGroupToDropdowns(LayerGroup group)
    {
        layerBehaviours.ForEach(layer =>
        {
            var dropdown = layer
                .GetComponentInChildren<MultiSelectDropdown>();
            var options = dropdown.Options;
            options.Add(group.GroupName, false);
            dropdown.UpdateDropdown(options);
        });
    }

    private void RemoveLayerGroupFromDropdowns(LayerGroup group)
    {
        layerBehaviours.ForEach(layer =>
        {
            var dropdown = layer.GetComponentInChildren<MultiSelectDropdown>();
            var options = dropdown.Options;
            options.Remove(group.GroupName);
            dropdown.UpdateDropdown(options);
        });
    }

    private void UpdateLayerGroupDropdowns()
    {
        layerBehaviours.ForEach(layerBehaviour =>
        {
            var dropdown = layerBehaviour.GetComponentInChildren<MultiSelectDropdown>();
            var options = new Dictionary<string, bool>();
            layerGroups.ForEach(group => options.Add(group.GroupName, layerBehaviour.Layer.groups.Contains(group.GroupName)));
            dropdown.UpdateDropdown(options);
        });
    }
}
