﻿using IfcEditorTypes;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Handles Assignment of Layers to IFC Elements and Annotaions.
/// </summary>
public class LayerSelectionButton : MonoBehaviour
{
    private Layer layer;

    private SelectTool selectTool;
    private AnnotateTool annotateTool;
    private IfcRepository ifcRepository;
    private TMPro.TMP_Text label;
    private Toggle toggle;

    public Layer Layer {
        get => layer;

        set {
            layer = value;
            if(label == null) label = GetComponentInChildren<TMPro.TMP_Text>();
            label.text = layer.name;
        }
    }

    private void Awake()
    {
        selectTool = FindObjectOfType<SelectTool>();
        annotateTool = FindObjectOfType<AnnotateTool>();
        selectTool.OnSelectionChange.AddListener(UpdateLayersForIfcElement);
        annotateTool.OnSelectionChange.AddListener(UpdateLayersForAnnotation);

        ifcRepository = IfcRepository.GetIfcRepository();
        label = GetComponentInChildren<TMPro.TMP_Text>();
        toggle = GetComponent<Toggle>();
    }

    private void UpdateLayersForAnnotation()
    {
        if (annotateTool.Editing == null) return;
        toggle.isOn = annotateTool.Editing.annotation.layers.Contains(layer);
    }

    private void UpdateLayersForIfcElement()
    {
        if (selectTool.CurrentSelection == null) return;
        IfcElement currentSelection = ifcRepository.GetIfcElementOfGameObject(selectTool.CurrentSelection);
        toggle.isOn = currentSelection.layers.Contains(layer);
    }

    public void AssignLayer(bool assignLayer)
    {
        if (annotateTool.enabled)
        {
            AssignLayerToAnnotation(assignLayer);
            return;
        }

        var selection = selectTool.CurrentSelection.GetComponentsInChildren<Transform>();
        foreach(var elem in selection)
        {
            IfcElement ifc = ifcRepository.GetIfcElementOfGameObject(elem.gameObject);
            if (ifc == null) continue;
            if (assignLayer)
            {
                if (!ifc.layers.Contains(Layer))
                {
                    ifc.layers.Add(Layer);
                    ifcRepository.UpdateIfcElement(ifc);
                    Layer.children.Add(elem);
                }
            }
            else
            {
                if (ifc.layers.Remove(Layer))
                {
                    Layer.children.Remove(elem);
                    ifcRepository.UpdateIfcElement(ifc);
                };
            }
        }
    }

    private void AssignLayerToAnnotation(bool assignLayer)
    {
        if (annotateTool.Editing == null) return;
        if (assignLayer)
        {
            annotateTool.Editing.annotation.layers.Add(layer);
        }
        else
        {
            annotateTool.Editing.annotation.layers.Remove(layer);
        }
    }

    internal void RefreshButtonLabel()
    {
        if (label == null) label = GetComponentInChildren<TMPro.TMP_Text>();
        label.text = layer.name;
    }
}
