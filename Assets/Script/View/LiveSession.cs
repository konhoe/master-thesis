﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Newtonsoft.Json;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using Newtonsoft.Json.Serialization;
using TMPro;
using System.Net.Sockets;
using UnityEngine.Events;

/**
 * TODO: Extract Message Classes
 * Errorhandling with Socketconnection
 * Audio
 * Split into Pointer, Selection & Audio Logic
 */
public class LiveSession : MonoBehaviour
{
    public GameObject pointerPrefab;
    public GameObject liveSessionHolder;
    public ToolButton selectionToolButton;
    public List<GameObject> supportedButtons;
    public List<GameObject> unsupportedButtons;

    private TextMeshProUGUI liveSessionBtnText;
    private string serverUri = "ws://localhost:3000";
    private WebSocketWrapper ws;
    private RaycastHit hit;
    private GameObject pointer;
    private LoaderManager loaderManager;
    private SelectTool selectTool;

    public bool LivePointer { get; set; }

    private void Start()
    {
        liveSessionHolder.SetActive(false);
        liveSessionBtnText = liveSessionHolder.GetComponentInChildren<TextMeshProUGUI>();

        loaderManager = FindObjectOfType<LoaderManager>();
        loaderManager.objectLoadedEvent.AddListener(() =>
        {
            if (loaderManager.projectName != null && !loaderManager.projectName.Equals(""))
                liveSessionHolder.SetActive(true);
        });
        loaderManager.closeEvent.AddListener(() =>
        {
            liveSessionHolder.SetActive(false);
            DoDeactivation();
        });

        selectTool = FindObjectOfType<SelectTool>();
    }

    public void ToggleSession()
    {
        if (ws == null)
            DoActivation();
        else
            DoDeactivation();
    }

    public void DoActivation()
    {
        if (ws != null) DoDeactivation();
        ws = WebSocketWrapper.Create(serverUri);
        ws.Connect();
        Debug.Log("Connecting to Socket...");
        ws.OnConnect((_) =>
        {
            UnityMainThreadDispatcher.Instance().Enqueue(() =>
            {
                Debug.Log("Connection successful!");
                unsupportedButtons.ForEach(g => g.SetActive(false));
                supportedButtons.ForEach(g => g.SetActive(true));
                selectionToolButton.Tool.Activate();
                
                liveSessionBtnText.text = "End Live Session";
                Canvas.ForceUpdateCanvases();
                ws.SendObject(new { type = "session", name = "Test", project = loaderManager.projectName });
            });
        });

    }

    public void DoDeactivation()
    {
        if (ws != null)
            ws.CloseSocketAsync();
        ws = null;
        ClearPointer();
        selectionToolButton.Tool.Activate();
        unsupportedButtons.ForEach(g => g.SetActive(true));
        supportedButtons.ForEach(g => g.SetActive(false));
        liveSessionBtnText.text = "Start Live Session";
    }

    void Update()
    {
        if (Input.GetMouseButtonDown(0) && !EventSystem.current.IsPointerOverGameObject() && LivePointer)
            CastLivePointerRay();
    }

    private void CastLivePointerRay()
    {
        ClearPointer();
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        if (Physics.Raycast(ray, out hit))
        {
            pointer = Instantiate(pointerPrefab, hit.point, Quaternion.FromToRotation(transform.up, hit.normal));
            var canvas = pointer.GetComponentInChildren<Canvas>();
            canvas.worldCamera = Camera.main;
            pointer.GetComponentInChildren<Button>().onClick.AddListener(() =>
            {
                var pointerMessage = new { type = "pointer", pointer.transform.position, pointer.transform.eulerAngles };
                ws.SendObject(pointerMessage);
                canvas.gameObject.SetActive(false);
            });
        }
    }

    public void SendSelection()
    {
        var selectionMessage = new { type = "selection", selectTool.CurrentSelection.name };
        ws.SendObject(selectionMessage);
    }

    private void ClearPointer()
    {
        if (pointer != null)
        {
            DestroyImmediate(pointer);
            pointer = null;
        }
    }
}
