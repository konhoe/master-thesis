﻿using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using SFB;
using System;
using System.Linq;
using Newtonsoft.Json;
using IfcEditorTypes;
using UnityEngine.SceneManagement;
using System.Net.Configuration;

/// <summary>
/// Handles Loading of IFC files and Projects.
/// </summary>
public class LoaderManager : MonoBehaviour {

    public GameObject objectRoot;

    [HideInInspector]
    public UnityEvent objectLoadedEvent = new UnityEvent();
    [HideInInspector]
    public UnityEvent closeEvent = new UnityEvent();
    [HideInInspector]
    public List<GameObject> loadedObjects = new List<GameObject>();
    [HideInInspector]
    public string projectName;

    [SerializeField]
    private Button closeButton, loadProjectButton;
    private List<string> loadedIfcPaths = new List<string>();
    private AnnotateTool annotateTool;
    private LayerManager layerManager;
    private IfcRepository ifcRepository;
    private ErrorHandler errorHandler;

    private void Start()
    {
        ifcRepository = IfcRepository.GetIfcRepository();
        annotateTool = FindObjectOfType<AnnotateTool>();
        layerManager = FindObjectOfType<LayerManager>();
        errorHandler = FindObjectOfType<ErrorHandler>();
    }

    public void SelectFile()
    {
        var path = StandaloneFileBrowser.OpenFilePanel("Open File", "", "ifc", false)[0];
        if (path == null || path == "" || !path.ToLower().EndsWith(".ifc"))
        {
            var msg = "Error: Invalid File selected.";
            Debug.Log(msg);
            errorHandler.ShowError(msg);
            return;
        }else if (loadedIfcPaths.Contains(path))
        {
            var msg = "Error: Can not load the same file twice.";
            Debug.Log(msg);
            errorHandler.ShowError(msg);
            return;
        }
        LoadIfcFromPath(path);
    }

    private void LoadIfcFromPath(string path)
    {
        LoadIfcFromPathWithoutNotify(path);
        objectLoadedEvent.Invoke();
        closeButton.interactable = true;
        loadProjectButton.interactable = false;
    }

    private void LoadIfcFromPathWithoutNotify(string path)
    {
        var newRoot = new GameObject(Path.GetFileName(path));
        newRoot.transform.parent = objectRoot.transform;

        var newObject = ifcRepository.LoadIfcFromPath(path, newRoot);
        loadedObjects.Add(newObject);
        loadedIfcPaths.Add(path);

        CenterRoot(newObject.transform);
        UniformScale(newObject.transform);
    }

    public void LoadProject(string path)
    {
        projectName = Path.GetFileNameWithoutExtension(path);
        var ifcFiles = Directory.GetFiles(path).Where(item => item.ToLower().EndsWith(".ifc"));
        foreach (var ifc in ifcFiles)
            LoadIfcFromPathWithoutNotify(ifc);

        //Parse JSON, apply Transform, Layers, LayerGroups and Annotations
        var jsonFile = Directory.GetFiles(path).Single(item => Path.GetFileName(item).StartsWith(projectName));
        string json = System.IO.File.ReadAllText(jsonFile);
        IfcExport import = JsonConvert.DeserializeObject<IfcExport>(json);

        //apply filtered Properties
        ifcRepository.filteredProperties = import.filteredProperties;

        //apply Root Transforms
        import.transforms.ForEach(toApply =>
        {
            var target = loadedObjects.Find(o => o.transform.parent.name == toApply.target).transform.parent;
            target.position = toApply.position;
            target.eulerAngles = toApply.rotation;
            target.localScale = toApply.scale;
        });

        //apply Layer references
        import.ifcElements.ForEach(ifcElement =>
        {
            //Map ifcElement.layer to import.layer List (same object references)
            List<Layer> referencedLayers = new List<Layer>();
            ifcElement.layers.ForEach(l =>
               {
                   var r = import.layers.Find(refLayer => refLayer.name == l.name);
                   referencedLayers.Add(r);
               });
            ifcElement.layers = referencedLayers;

            //Update children of layer
            if (ifcElement.layers.Count > 0)
            {
                var child = GetGameObjectById(ifcElement.id).transform;
                ifcRepository.UpdateIfcElement(ifcElement);
                ifcElement.layers.ForEach(l =>
                {
                    l.children.Add(child);
                });
            }
        });

        //apply inActive properties and deleted flag
        import.ifcElements.ForEach(ifcElement =>
        {
            if(ifcElement.properties.Exists(p => !p.isActive))
                ifcRepository.UpdateIfcElement(ifcElement);
            if (ifcElement.deleted)
                Destroy(GetGameObjectById(ifcElement.id));
        });

        import.annotations.ForEach(a =>
        {
            //Map relative mediaFile to absolute file Path.
            if (!string.IsNullOrEmpty(a.media))
                a.media = Path.Combine(path, a.media);
            annotateTool.AddAnnotation(a);
        });

        layerManager.Initialize(import.layers, import.layerGroups);
        objectLoadedEvent.Invoke();
        closeButton.interactable = true;
        loadProjectButton.interactable = false;
    }

    public GameObject GetGameObjectById(string id)
    {
        foreach(var g in loadedObjects)
        {
            Transform[] children = g.GetComponentsInChildren<Transform>();
            for (int i = 0; i < children.Length; i++)
            {
                if (children[i].name.Contains(id))
                    return children[i].gameObject;
            }
        }
        Debug.Log(id + " not found!");
        return null;
    }

    private void CenterRoot(Transform root)
    {
        Vector3 center = new Vector3();
        if (root.childCount > 0)
        {
            Renderer[] allChildren = root.GetComponentsInChildren<Renderer>();
            foreach (Renderer child in allChildren)
            {
                center += child.bounds.center;
            }
            center /= allChildren.Length;
        }
        root.position -= new Vector3(center.x, 0, center.z);
    }

    private void UniformScale(Transform root)
    {
        Bounds bounds = new Bounds();
        if (root.childCount == 0) return;
        Renderer[] allChildren = root.GetComponentsInChildren<Renderer>();
        foreach (Renderer child in allChildren)
        {
            bounds.Encapsulate(child.bounds);
        }
        var avgBound = (bounds.size.x + bounds.size.y + bounds.size.z) / 3;
        ScaleMeshes(root, avgBound);
        Debug.Log("Scaling object by " + root.parent.localScale);
    }

    private void ScaleMeshes(Transform root, float scale)
    {
        MeshFilter[] allChildren = root.GetComponentsInChildren<MeshFilter>();
        foreach (MeshFilter child in allChildren)
        {
            var original = child.sharedMesh.vertices;
            var vertices = new Vector3[child.sharedMesh.vertices.Length];
            for (var i = 0; i < vertices.Length; i++)
            {
                vertices[i] = original[i] / scale;
            }
            child.sharedMesh.vertices = vertices;
            child.sharedMesh.RecalculateNormals();
            child.sharedMesh.RecalculateBounds();
            child.sharedMesh.Optimize();
            child.GetComponent<MeshCollider>().sharedMesh = child.sharedMesh;
            child.mesh = child.sharedMesh;
        }
        Transform[] children = root.GetComponentsInChildren<Transform>();
        foreach(var t in children){
            t.localPosition /= scale;
        }
    }

    public string[] GetIfcPaths()
    {
        return loadedIfcPaths.ToArray();
    }

    /// <summary>
    /// Reload the main (first) scene
    /// </summary>
    public void CloseProject()
    {
        projectName = null;
        closeEvent.Invoke();
        ifcRepository.ResetRepository();
        SceneManager.LoadScene(0);
    }
}
