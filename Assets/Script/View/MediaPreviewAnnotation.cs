﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;
using UnityEngine.Video;

/// <summary>
/// Handles showing the attached media of an Annotaion.
/// </summary>
public class MediaPreviewAnnotation : MonoBehaviour
{
    [SerializeField]
    private RawImage preview;
    [SerializeField]
    private VideoPlayer videoPlayer;

    private List<string> imageExtension = new List<string> { ".jpg", ".jpeg", ".png", ".gif" };
    private List<string> videoExtension = new List<string> { ".mp4" };

    public void LoadPreview(string path)
    {
        gameObject.SetActive(true);
        StartCoroutine(LoadMedia(path));
    }

    public void ClearPreview()
    {
        preview.texture = null;
        videoPlayer.Stop();
        gameObject.SetActive(false);
    }

    private IEnumerator LoadMedia(string path)
    {
        var ext = Path.GetExtension(path);
        if (imageExtension.Contains(ext.ToLower()))
        {
            Debug.Log("MediaFile is an Image");

            using (UnityWebRequest loader = UnityWebRequestTexture.GetTexture("file://" + path))
            {
                yield return loader.SendWebRequest();
                if (string.IsNullOrEmpty(loader.error))
                {
                    var tex = DownloadHandlerTexture.GetContent(loader);
                    var sprite = Sprite.Create(tex, new Rect(Vector2.zero, new Vector2(tex.width, tex.height)), new Vector2(0.5f, 0.5f));
                    preview.texture = sprite.texture;
                    Debug.Log("Finished Loading Image");
                }
            }
        }
        else if (videoExtension.Contains(ext.ToLower()))
        {
            Debug.Log("MediaFile is a Video");
            preview.texture = videoPlayer.targetTexture;
            videoPlayer.url = "file://" + path;
            videoPlayer.prepareCompleted += (vid) =>
                {
                    videoPlayer.time = 1.0f;
                    videoPlayer.Play();
                    videoPlayer.Pause();
                    Debug.Log("Finished Loading Video.");
                };
            videoPlayer.Prepare();
        }
    }
}
