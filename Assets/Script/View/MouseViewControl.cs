﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Handles navigating around the loaded Objects. Rotation around target with right-click, switching to a new target with Key 'F'.
/// </summary>
public class MouseViewControl : MonoBehaviour
{
    public Transform targetTransform;
    public Vector3 targetOffset;
    public float distance = 5.0f;
    public float maxDistance = 20;
    public float minDistance = .6f;
    public float xSpeed = 200.0f;
    public float ySpeed = 200.0f;
    public int yMinLimit = -80;
    public int yMaxLimit = 80;
    public int zoomRate = 40;
    public float zoomDampening = 5.0f;

    private Vector3 targetVector;
    private float xDeg = 0.0f;
    private float yDeg = 0.0f;
    private float currentDistance;
    private float desiredDistance;
    private Quaternion currentRotation;
    private Quaternion desiredRotation;
    private Quaternion rotation;
    private Vector3 position;
    private SelectTool selectTool;
    private LoaderManager loaderManager;

    public void Start()
    {
        selectTool = FindObjectOfType<SelectTool>();
        loaderManager = FindObjectOfType<LoaderManager>();
        distance = Vector3.Distance(transform.position, targetTransform.position);
        currentDistance = distance;
        desiredDistance = distance;

        //be sure to grab the current rotations as starting points.
        position = transform.position;
        rotation = transform.rotation;
        currentRotation = transform.rotation;
        desiredRotation = transform.rotation;

        xDeg = -Vector3.Angle(Vector3.right, transform.right);
        yDeg = Vector3.Angle(Vector3.up, transform.up);
    }

    void LateUpdate()
    {
        //Skip all Input this frame if mouse is over UI
        if (UnityEngine.EventSystems.EventSystem.current.IsPointerOverGameObject())
        {
            return;
        }

        //Focus on Selection
        if (Input.GetKeyDown(KeyCode.F) && selectTool.CurrentSelection != null)
        {
            var newTarget = selectTool.CurrentSelection.transform.position;
            var renderer = selectTool.CurrentSelection.GetComponent<Renderer>();

            if(renderer != null)
            {
                newTarget = renderer.bounds.center;
            }

            if (targetVector != newTarget)
            {
                targetTransform = null;
                targetVector = newTarget;
            }
            else
            {
                targetTransform = loaderManager.objectRoot.transform;
            }

        }else if (Input.GetKeyDown(KeyCode.F))
        {
            targetTransform = loaderManager.objectRoot.transform;
        }

        // If right mouse => ORBIT
        if (Input.GetMouseButton(1))
        {
            xDeg += Input.GetAxis("Mouse X") * xSpeed * 0.02f;
            yDeg -= Input.GetAxis("Mouse Y") * ySpeed * 0.02f;

            desiredRotation = Quaternion.Euler(yDeg, xDeg, 0);
            currentRotation = transform.rotation;

            rotation = Quaternion.Lerp(currentRotation, desiredRotation, Time.deltaTime * zoomDampening);
            transform.rotation = rotation;
        }

        desiredDistance -= Input.GetAxis("Mouse ScrollWheel") * Time.deltaTime * zoomRate * Mathf.Abs(desiredDistance);
        desiredDistance = Mathf.Clamp(desiredDistance, minDistance, maxDistance);
        currentDistance = Mathf.Lerp(currentDistance, desiredDistance, Time.deltaTime * zoomDampening);

        if(targetTransform != null)
        {
            position = targetTransform.position - (rotation * Vector3.forward * currentDistance + targetOffset);
        }
        else
        {
            position = targetVector - (rotation * Vector3.forward * currentDistance + targetOffset);
        }

        transform.position = position;
    }
}