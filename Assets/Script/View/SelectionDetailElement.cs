﻿using IfcEditorTypes;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Represents a IFC Property in the Selection Detail view. Allows toggling this IFC Property.
/// </summary>
public class SelectionDetailElement : MonoBehaviour
{
    [SerializeField]
    private Toggle propertyToggle;

    [SerializeField]
    private TextMeshProUGUI text;

    private SelectionDetails selectionDetails;
    private IfcProperty ifcProperty;
    private IfcElement parent;
    private IfcRepository ifcRepository;

    public void Start()
    {
        ifcRepository = IfcRepository.GetIfcRepository();
    }

    public void Initialize(IfcElement parent, IfcProperty prop, SelectionDetails selectionDetails)
    {
        this.selectionDetails = selectionDetails;
        this.ifcProperty = prop;
        this.parent = parent;
        propertyToggle.SetIsOnWithoutNotify(prop.isActive);
        text.color = prop.isActive ? Color.black : Color.grey;
    }

    public void ToggleActive()
    {
        if (ifcProperty == null) return;
        SetState(!ifcProperty.isActive);
    }

    public void SetState(bool state)
    {
        if (ifcProperty == null) return;
        ifcProperty.isActive = state;
        ifcRepository.UpdateIfcElement(parent);
        selectionDetails.Refresh();
    }
}
