﻿using IfcEditorTypes;
using SFB;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Networking;
using UnityEngine.UI;
using UnityEngine.Video;

/// <summary>
/// Manages adding, changing and removing Annotaions.
/// </summary>
public class AnnotateTool : Tool
{
    [SerializeField]
    private GameObject flagPrefab, ballPrefab, layerSelectionHolder, layerSelectionPrefab, deleteBtn, slider;
    [SerializeField]
    private TMPro.TMP_Text sliderValue;
    [SerializeField]
    private TMPro.TMP_InputField title, content, mediaPath;
    [SerializeField]
    private MediaPreviewAnnotation mediaPreview;
    [SerializeField]
    private Vector3Input positionInput;
    [SerializeField]
    private int maxFileSizeInMB;

    private GameObject annotationPrefab;

    public List<Annotation> AllAnnotations { get; } = new List<Annotation>();
    public AnnotationComponent Editing { get; private set; }

    public UnityEvent OnSelectionChange = new UnityEvent();

    private GameObject tempAnnotation;
    private bool showTemp;
    private Color preSelectColor;

    private int exceptAnnotaions;
    private int onlyAnnotations;

    private void Awake()
    {
        annotationPrefab = flagPrefab;
        exceptAnnotaions = ~LayerMask.GetMask("Annotation");
        onlyAnnotations = LayerMask.GetMask("Annotation");
        slider.GetComponent<Slider>().onValueChanged.AddListener((value) =>
           {
               sliderValue.text = value.ToString("F");
               var scale = new Vector3(value, value, value);
               if (Editing != null)
               {
                   Editing.transform.localScale = scale;
                   Editing.annotation.size = value;
               }
               else
               {
                   tempAnnotation.transform.localScale = scale;
                   tempAnnotation.GetComponent<AnnotationComponent>().annotation.size = value;
               }
           });
    }

    protected override void DoActivation()
    {
        this.enabled = true;
        tempAnnotation = Instantiate(annotationPrefab);
        tempAnnotation.SetActive(false);
        Camera.main.cullingMask |= LayerMask.GetMask("Annotation");
    }

    protected override void DoDeactivation()
    {
        this.enabled = false;
        mediaPreview.ClearPreview();
        Destroy(tempAnnotation);
        tempAnnotation = null;
        Deselect();
        Camera.main.cullingMask &= ~LayerMask.GetMask("Annotation");
    }

    public void SelectMedia()
    {
        var path = StandaloneFileBrowser.OpenFilePanel("Open File", "", "", false)[0];
        if (path == null || path == "")
            return;
        float sizeInMB = new FileInfo(path).Length / 1000000.0f;
        if (sizeInMB > maxFileSizeInMB)
        {
            mediaPath.text = "ERROR: File too large! (>100 MB)";
            return;
        }

        mediaPath.text = path;
        Editing.annotation.media = path;
        mediaPreview.LoadPreview(path);
    }

    void Update()
    {
        //Skip all Input this frame if mouse is over UI
        if (UnityEngine.EventSystems.EventSystem.current.IsPointerOverGameObject())
            return;

        RaycastHit hit;
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

        //Cursor on existing Annotation => Hide temporary Annotation
        if (Physics.Raycast(ray, out hit, float.PositiveInfinity, onlyAnnotations))
            showTemp = false;
        else
            showTemp = true;
            
        if (showTemp && Physics.Raycast(ray, out hit, float.PositiveInfinity, exceptAnnotaions))
        {
            UpdateTempAnnotation(hit.point);
        }
        else
        {
            tempAnnotation.SetActive(false);
        }

        //Click => new Annotation or Update existing Annotation
        if (Input.GetMouseButtonDown(0))
        {
            if (hit.collider == null)
            {
                Deselect();
                return;
            }
            if (showTemp)
            {
                var newAnnotation = new Annotation();
                newAnnotation.position = tempAnnotation.transform.position;
                newAnnotation.size = slider.GetComponent<Slider>().value;
                newAnnotation.parent = hit.collider.gameObject.name;
                newAnnotation.visual = annotationPrefab == ballPrefab ? "ball" : "flag";
                var component = tempAnnotation.GetComponent<AnnotationComponent>();
                tempAnnotation.transform.parent = hit.collider.gameObject.transform;
                component.annotation = newAnnotation;

                Select(component);
                component.GetComponentInChildren<Collider>().enabled = true;
                AllAnnotations.Add(newAnnotation);

                //Instantiate new temporary Annotation that follows cursor
                tempAnnotation = Instantiate(annotationPrefab);
            }
            else
            {
                var clicked = hit.transform.GetComponent<AnnotationComponent>();
                Select(clicked);
            }
        }
           
    }

    internal void AddAnnotation(Annotation a)
    {
        var visual = a.visual == "ball" ? ballPrefab : flagPrefab;
        var component = Instantiate(visual).GetComponent<AnnotationComponent>();
        component.annotation = a;
        component.GetComponentInChildren<TMPro.TMP_Text>().text = a.title;
        component.GetComponent<Collider>().enabled = true;
        component.transform.position = a.position;
        component.transform.localScale = new Vector3(a.size, a.size, a.size);
        
        var parent = GameObject.Find(a.parent);
        if(parent != null)
            component.transform.parent = parent.transform;

        AllAnnotations.Add(a);
    }

    internal void AddLayer(Layer layer)
    {
        var layerSelectionBtn = Instantiate(layerSelectionPrefab, layerSelectionHolder.transform).GetComponent<LayerSelectionButton>();
        layerSelectionBtn.Layer = layer;
    }

    internal void RemoveLayer(Layer layer)
    {
        Destroy(layerSelectionHolder.GetComponentsInChildren<LayerSelectionButton>().Single(l => l.Layer == layer).gameObject);
        AllAnnotations.ForEach(a =>
            a.layers.Remove(layer)
        );
    }

    internal void UpdateLayerList()
    {
        foreach (Transform child in layerSelectionHolder.transform)
        {
            var layerSelection = child.GetComponent<LayerSelectionButton>();
            layerSelection.RefreshButtonLabel();
        }
    }

    private void UpdateTempAnnotation(Vector3 point)
    {
        tempAnnotation.SetActive(true);
        tempAnnotation.transform.position = point;
    }

    private void Select(AnnotationComponent toShow)
    {
        Deselect();
        Editing = toShow;
        deleteBtn.SetActive(true);
        slider.GetComponent<Slider>().value = Editing.transform.localScale.x;

        var selected = toShow.annotation;
        title.text = selected.title;
        content.text = selected.content;
        mediaPath.text = selected.media;
        if (!string.IsNullOrEmpty(selected.media))
            mediaPreview.LoadPreview(selected.media);

        var sceneText = toShow.GetComponentInChildren<TMPro.TMP_Text>();
        sceneText.text = selected.title;

        title.onEndEdit.AddListener((t) => sceneText.text = selected.title = t);
        content.onEndEdit.AddListener((c) => selected.content = c);

        var renderer = toShow.GetComponentInChildren<Renderer>();
        preSelectColor = renderer.material.color;
        renderer.material.color = Color.red;

        positionInput.SetVector(Editing.transform.position);
        positionInput.valuesChanged.AddListener(() =>
        {
            Editing.transform.position = positionInput.GetVector();
            Editing.annotation.position = Editing.transform.position;
        });

        OnSelectionChange.Invoke();
    }

    private void Deselect()
    {
        if (Editing != null)
        {
            Editing.GetComponentInChildren<Renderer>().material.color = preSelectColor;
        }
        positionInput.SetVector(Vector3.zero);
        positionInput.valuesChanged.RemoveAllListeners();
        title.onEndEdit.RemoveAllListeners();
        content.onEndEdit.RemoveAllListeners();
        deleteBtn.SetActive(false);
        slider.GetComponent<Slider>().SetValueWithoutNotify(0.4f);
        mediaPreview.ClearPreview();
        mediaPath.text = "";
        Editing = null;
    }

    public void DeleteCurrentAnnotation()
    {
        AllAnnotations.Remove(Editing.annotation);
        Destroy(Editing.gameObject);
        Editing = null;
        Deselect();
    }

    public void DeleteMedia()
    {
        Editing.annotation.media = null;
        mediaPath.text = "";
        mediaPreview.ClearPreview();
    }

    /// <summary>
    /// Switch annotation to another visual, currently "ball" or "flag"
    /// </summary>
    /// <param name="visual"></param>
    public void SelectedSwitchVisual(string visual)
    {
        if (Editing != null)
        {
            var toSpawn = visual == "ball" ? ballPrefab : flagPrefab;
            var annotation = Editing.annotation;
            annotation.visual = visual;
            var newAnnotation = Instantiate(toSpawn, Editing.transform.position, Editing.transform.rotation, Editing.transform.parent);
            var annotationHolder = newAnnotation.GetComponent<AnnotationComponent>();
            annotationHolder.annotation = annotation;
            newAnnotation.GetComponent<Collider>().enabled = true;
            DestroyImmediate(Editing.gameObject);
            Select(annotationHolder);
        }
        else
        {
            annotationPrefab = visual == "ball" ? ballPrefab : flagPrefab;
            DoDeactivation();
            DoActivation();
        }
        
    }
}
