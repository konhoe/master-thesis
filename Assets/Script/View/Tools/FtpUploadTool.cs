﻿using FluentFTP;
using IfcEditorTypes;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json.Serialization;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Handles the Upload UI.
/// </summary>
public class FtpUploadTool : Tool
{
    [SerializeField]
    private TMPro.TMP_InputField exportName;
    [SerializeField]
    private Button uploadButton;
    [SerializeField]
    private TMPro.TMP_Text uploadButtonText;
    [SerializeField]
    private Transform ftpFolderButtonHolder;
    [SerializeField]
    private GameObject ftpFolderButtonPrefab;

    private FTPRepository ftp;
    private IfcRepository ifcRepository;
    private LoaderManager loaderManager;
    private AnnotateTool annotateTool;
    private LayerManager layerManager;
    private double progress = 0;

    private void Start()
    {
        ftp = FTPRepository.GetFTPRepository();
        ifcRepository = IfcRepository.GetIfcRepository();
        loaderManager = FindObjectOfType<LoaderManager>();
        annotateTool = FindObjectOfType<AnnotateTool>();
        layerManager = FindObjectOfType<LayerManager>();
        exportName.onValidateInput = (text, charIndex, charAdded) =>
            {
                return charAdded < 128 && charAdded != '/' && charAdded != '\\' ? charAdded : '\0'; // Check that only Ascii (first 127 chars) are entered and no path is defined
            };
    }

    /// <summary>
    /// Gets called from a scene Button and start the async Upload in a coroutine.
    /// </summary>
    public void Upload()
    {
        StartCoroutine(AsyncUpload());
    }

    private IEnumerator AsyncUpload()
    {
        uploadButton.interactable = false;
        uploadButtonText.text = "Collecting...";
        yield return 0;

        var project = exportName.text;

        IfcExport ifcExport = ifcRepository.XmlToIfcJsonCollection(loaderManager.loadedObjects);

        //Update all Annotations with their final world positions
        var annotationComponents = loaderManager.objectRoot.GetComponentsInChildren<AnnotationComponent>(true);
        foreach(var c in annotationComponents)
            c.annotation.position = c.transform.position;

        //Add Annotations & Remove absolute Paths from Annotations
        ifcExport.annotations = annotateTool.AllAnnotations.Where(a => a.title != "")
            .Select(a => PrepExportAnnotation(a)).ToList();
        
        //Add Transform Changes
        ifcExport.transforms = loaderManager.loadedObjects.Select(o => new IfcTransform(o.transform.parent)).ToList();

        //Collect Files (ifc and media Files)
        var files = loaderManager.GetIfcPaths().ToList();
        files.AddRange(annotateTool.AllAnnotations.Where(a => a.title != "").Select(i => i.media));

        //Add Layers
        ifcExport.layers = layerManager.GetLayers();

        //Add LayerGroups ( This List also represents the sequence of LayerGroupButtons )
        ifcExport.layerGroups = layerManager.GetLayerGroups();

        //List of globally filtered IFC Properties
        ifcExport.filteredProperties = ifcRepository.filteredProperties;

        //Generate JSON
        var settings = new JsonSerializerSettings()
        {
            ReferenceLoopHandling = ReferenceLoopHandling.Ignore,
            ContractResolver = new CamelCasePropertyNamesContractResolver(),
            Converters = new[] { new Vector3Converter() }
        };
        var jsonString = JsonConvert.SerializeObject(ifcExport, Formatting.None, settings);
        var json = Path.Combine(Path.GetTempPath(), project + ".json");
        File.WriteAllText(json, jsonString);
        files.Add(json);

        uploadButtonText.text = "Uploading...";
        yield return 0;
        var task = ftp.UploadProjectAsync(project, files, (p) => progress = p);
        while (!task.IsCompleted)
        {
            uploadButtonText.text = ""+progress.ToString("F0")+"%";
            yield return 0;
        }
        progress = 0;
        RefreshButtons();
        uploadButtonText.text = "Done!";
        yield return new WaitForSeconds(1.0f);
        uploadButtonText.text = "Export";
        uploadButton.interactable = true;
    }

    private Annotation PrepExportAnnotation(Annotation a)
    {
        var clone = new Annotation();
        clone.title = a.title;
        clone.content = a.content;
        clone.parent = a.parent;
        clone.visual = a.visual;
        clone.media = Path.GetFileName(a.media);
        clone.position = a.position;
        clone.size = a.size;
        clone.layers = a.layers;

        return clone;
    }

    protected override void DoActivation()
    {
        RefreshButtons();
    }

    protected override void DoDeactivation()
    {
        for (int i = 0; i < ftpFolderButtonHolder.childCount; i++)
        {
            Destroy(ftpFolderButtonHolder.GetChild(i).gameObject);
        }
        uploadButton.interactable = true;
        uploadButtonText.text = "Export";
    }

    private void SpawnButton(string name, string date)
    {
        var fullName = $"{name} - <i>{date}</i>";
        var button = Instantiate(ftpFolderButtonPrefab, ftpFolderButtonHolder).transform.Find("FolderButton").GetComponent<Button>();
        var nameField = button.transform.Find("Name").GetComponent<TMPro.TMP_Text>();
        nameField.text = fullName;
        button.onClick.AddListener(() => SetProjectName(name));
        var delete = button.transform.parent.Find("Delete").GetComponent<Button>();
        delete.onClick.AddListener(() => DeleteProject(name));
    }

    private void DeleteProject(string project)
    {
        ftp.DeleteRemoteProject(project);
        RefreshButtons();
    }

    private void RefreshButtons()
    {
        //Delete Existing Buttons
        for (int i = 0; i < ftpFolderButtonHolder.childCount; i++)
            Destroy(ftpFolderButtonHolder.GetChild(i).gameObject);
        
        var directories = ftp.GetDirectoryItems();
        foreach (var dir in directories.OrderByDescending(dir => dir.Modified))
            SpawnButton(dir.Name, dir.Modified.ToString("dd.MM.yyyy"));
    }

    private void SetProjectName(string projectName)
    {
        exportName.text = projectName;
    }
}

//Custom Converter for Vec3 since default Convert would include magnitude, normalized and other computed values.
public class Vector3Converter : JsonConverter<Vector3>
{
    public override void WriteJson(JsonWriter writer, Vector3 value, JsonSerializer serializer)
    {
        writer.WriteStartObject();
        writer.WritePropertyName("x");
        writer.WriteValue(value.x);
        writer.WritePropertyName("y");
        writer.WriteValue(value.y);
        writer.WritePropertyName("z");
        writer.WriteValue(value.z);
        writer.WriteEndObject();
    }

    public override Vector3 ReadJson(JsonReader reader, Type objectType, Vector3 existingValue, bool hasExistingValue, JsonSerializer serializer)
    {
        JObject vec = (JObject)reader.Value;

        return new Vector3((float)vec.GetValue("x"), (float)vec.GetValue("y"), (float)vec.GetValue("z"));
    }
}