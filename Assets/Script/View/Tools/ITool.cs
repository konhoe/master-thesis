﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// A functionality of the Editor with an associated UI.
/// </summary>
public interface ITool
{
    /// <summary>
    /// Activate this tool and initialize its functionality.
    /// </summary>
    void Activate();

    // <summary>
    /// Deactivate this tool and clean up.
    /// </summary>
    void Deactivate();
}
