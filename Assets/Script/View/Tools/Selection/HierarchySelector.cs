﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.SceneManagement;
using System.Linq;
using System.Xml.Linq;
using UnityEngine.UI;

/// <summary>
/// Provides selection capabilities in the Hierarchy View.
/// </summary>
public class HierarchySelector: MonoBehaviour, ISelector {

    private SelectTool selectTool;
    private HierarchyView hierarchyView;

    void Start () {
        selectTool = FindObjectOfType<SelectTool>();
        hierarchyView = FindObjectOfType<HierarchyView>();
    }

    public void ExternalSetSelected(GameObject g, bool single)
    {
        hierarchyView.UnmarkAll();
        if (g == null) return;
        hierarchyView.MarkActive(g, single);
    }

    public void Select(GameObject reference)
    {
        selectTool.CurrentSelection = reference;
    }

}
