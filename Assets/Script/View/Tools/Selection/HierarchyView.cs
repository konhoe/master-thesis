﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Xml.Linq;
using System;
using System.Linq;

/// <summary>
/// Fills the Hierarchy view with IFC Elements and handles expanding / collapsing of Hierarchy Elements.
/// </summary>
public class HierarchyView : MonoBehaviour
{
    public ScrollRect hierarchyScrollRect;
    public GameObject hierarchyElement;
    public Color highlightColor;

    private GameObject hierarchyHolder;
    private IfcRepository ifcRepository;
    private LoaderManager loaderManager;
    private SelectTool selectTool;
    private string topLevelElementID;
    private const int INDENT = 10;

    // Start is called before the first frame update
    void Start()
    {
        hierarchyHolder = hierarchyScrollRect.content.gameObject;
        ifcRepository = IfcRepository.GetIfcRepository();
        loaderManager = FindObjectOfType<LoaderManager>();
        loaderManager.objectLoadedEvent.AddListener(InitialHierarchy);
        selectTool = FindObjectOfType<SelectTool>();
    }

    internal void Expand(string id)
    {
        var toExpand = ifcRepository.XElementFromID(id);
        if(id == topLevelElementID && toExpand.Parent.Name != "decomposition")
        {
            toExpand = toExpand.Parent;
            topLevelElementID = toExpand.Attribute("id").Value;
            selectTool.CurrentSelection = null;
        }
        else
        {
            topLevelElementID = id;
        }
        ClearHierarchy();
        SpawnHierarchy(toExpand);
    }

    public void MarkActive(GameObject g, bool single)
    {
        var id = ifcRepository.GetIdFromGameObjectName(g.name);
        topLevelElementID = null;
        Expand(id);

        var hierarchyElem = hierarchyHolder.GetComponentsInChildren<HierarchyElement>().Single(elem => elem.id == id);
        if (single)
        {
            hierarchyElem.background.color = highlightColor;
        }

        SnapTo(hierarchyElem.GetComponent<RectTransform>());
        ColorSelection(hierarchyElem, highlightColor);
    }

    public void UnmarkAll()
    {
        foreach (var he in hierarchyHolder.GetComponentsInChildren<HierarchyElement>())
            he.background.color = Color.white;
    }

    private void InitialHierarchy()
    {
        var xElements = ifcRepository.loadedXmls[0].Descendants("decomposition").Elements();
        foreach (var x in xElements)
        {
            SpawnHierarchy(x);
        }
    }

    private void ClearHierarchy()
    {
        hierarchyHolder.GetComponentsInChildren<HierarchyElement>();
        //Clear old Hierarchy
        foreach (var h in hierarchyHolder.GetComponentsInChildren<HierarchyElement>())
        {
            h.gameObject.SetActive(false);
            DestroyImmediate(h.gameObject);
        }
    }

    private void SpawnHierarchy(XElement root)
    {
        var id = root.Attribute("id")?.Value;
        var ifcElem = ifcRepository.FindByID(id);
        if (id == null || ifcElem.deleted)
            return;

        SpawnHierarchyElement(0, root, true);

        foreach (var child in root.Elements())
        {
            var childId = child.Attribute("id")?.Value;
            if (childId == null) continue;
            var childIfcElem = ifcRepository.FindByID(childId);
            if (childIfcElem.deleted) continue;

            SpawnHierarchyElement(1, child, false);
        }
        var childCount = hierarchyHolder.transform.childCount;

        //Adjust height of ScrollRect to fit content
        var holderRect = hierarchyHolder.GetComponent<RectTransform>();
        holderRect.sizeDelta = new Vector2(holderRect.sizeDelta.x, childCount * hierarchyElement.GetComponent<RectTransform>().sizeDelta.y);
    }

    private void SpawnHierarchyElement(int depth, XElement root, bool isTopLevel)
    {
        var listItem = Instantiate(hierarchyElement, hierarchyHolder.transform);
        var textChild = listItem.GetComponentInChildren<TMPro.TextMeshProUGUI>();
        var he = listItem.GetComponent<HierarchyElement>();
        he.id = root.Attribute("id").Value;
        he.IsToplevel = isTopLevel;
        he.HasChildren = HasChildren(root);

        if (root.Attribute("Name") != null && root.Attribute("Name").Value != "")
        {
            textChild.text = root.Attribute("Name").Value;
            if (textChild.text.Length > 30)
                textChild.text = textChild.text.Substring(0, 30) + "...";
        }
        else
        {
            textChild.text = "Unnamed";
        }
        listItem.name = textChild.text; //set name in Unity Hierarchy aswell

        //Offset Text by hierarchy depth
        Vector2 leftRight = textChild.GetComponent<RectTransform>().offsetMin;
        leftRight += new Vector2(INDENT * depth, 0);
        textChild.GetComponent<RectTransform>().offsetMin = leftRight;
    }

    //Color all Objects and subObjects in hierarchy
    private void ColorSelection(HierarchyElement root, Color newColor)
    {
        foreach (var he in root.GetComponentsInChildren<HierarchyElement>())
            he.background.color = newColor;
    }

    //https://stackoverflow.com/questions/30766020/how-to-scroll-to-a-specific-element-in-scrollrect-with-unity-ui
    public void SnapTo(RectTransform child)
    {
        Canvas.ForceUpdateCanvases();
        Vector2 viewportLocalPosition = hierarchyScrollRect.viewport.localPosition;
        Vector2 childLocalPosition = child.localPosition;
        Vector2 result = new Vector2(
            0,
            0 - (viewportLocalPosition.y + childLocalPosition.y)
        );
        hierarchyScrollRect.content.localPosition = result;
    }

    private bool HasChildren(XElement root)
    {
        foreach (var subChild in root.Elements())
        {
            if (ifcRepository.IsReference(subChild)) continue;
            return true;
        }
        return false;
    }

}
