﻿using UnityEngine;

/// <summary>
/// Interface for all selection methods.
/// </summary>
public interface ISelector
{
    void ExternalSetSelected(GameObject g, bool single);
}
