﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

/// <summary>
/// Handles selection in the scene through left clicking on components.
/// </summary>
public class RaycastSelector: MonoBehaviour, ISelector {

    private int depth = 0;
    private SelectTool selectTool;
    private Color originalColor;
    private LoaderManager loader;
    private LiveSession liveSession;
    private IEnumerable<Material> marked;

    private void Start()
    {
        selectTool = FindObjectOfType<SelectTool>();
        loader = FindObjectOfType<LoaderManager>();
        liveSession = FindObjectOfType<LiveSession>();
    }

    public void ExternalSetSelected(GameObject g, bool single)
    {
        MarkDeselected(marked);
        if (g == null) return;
        if (single)
        {
            var mesh = g.GetComponent<MeshRenderer>();
            if (mesh != null)
                MarkSelected(new List<Material> { mesh.material });
        }
        else
        {
            var toSelect = g.GetComponentsInChildren<MeshRenderer>().Select(m => m.material);
            MarkSelected(toSelect);
        }

    }

    void Update()
    {
        if (Input.GetMouseButtonDown(0) && !EventSystem.current.IsPointerOverGameObject() && !liveSession.LivePointer)
            CastRay();
    }

    private void CastRay()
    {
        List<Transform> hits = new List<Transform>();
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

        //Order List by distance to Camera and get List of Transforms
        hits = Physics.RaycastAll(ray).OrderBy(x => x.distance).Select(elem => elem.transform).ToList();
        if (hits.Count == 0)
        {
            depth = 0;
            selectTool.CurrentSelection = null;
            return;
        }

        if (selectTool.CurrentSelection != null && hits.Contains(selectTool.CurrentSelection.transform))
            depth = Mathf.Clamp(depth + 1, 0, hits.Count - 1);
        else
            depth = 0;

        selectTool.CurrentSelection = hits[depth].gameObject;
    }

    protected void MarkSelected(IEnumerable<Material> toMark)
    {
        if (toMark == null) return;

        foreach (var m in toMark)
        {
            originalColor = m.color;
            m.SetColor("_EmissionColor", Color.red);
            m.EnableKeyword("_EMISSION");
        }
        marked = toMark;
    }

    protected void MarkDeselected(IEnumerable<Material> toUnmark)
    {
        if (toUnmark == null) return;
        foreach (var m in toUnmark)
        {
            m.SetColor("_EmissionColor", Color.white);
            m.DisableKeyword("_EMISSION");
        }
    }
}
