﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

/// <summary>
/// Handles the selection and deselection of Gameobjects/IFCElements.
/// </summary>
public class SelectTool : Tool {

    public UnityEvent OnSelectionChange;

    private ISelector[] selectors;
    private SelectionDetails selectionDetails;
    private GameObject currentSelection;
    private LoaderManager loader;

    public GameObject CurrentSelection
    {
        get
        {
            return currentSelection;
        }

        set
        {
            // Selection is a Mapped Item without id => select parent
            if (value != null && value.name.StartsWith("IfcMappedItem"))
            {
                currentSelection = value.transform.parent.gameObject;
                UpdateSelectors(currentSelection, false);
                selectionDetails.UpdateSelectionDetails(currentSelection);
                return;
            }
            bool singleSelection = currentSelection == value;
            currentSelection = value;
            UpdateSelectors(currentSelection, singleSelection);
            selectionDetails.UpdateSelectionDetails(currentSelection);
            OnSelectionChange.Invoke();
        }
    }

    private void UpdateSelectors(GameObject newSelection, bool singleSelection)
    {
        foreach(var selector in selectors)
            selector.ExternalSetSelected(newSelection, singleSelection);
    }

    private void Start()
    {
        loader = FindObjectOfType<LoaderManager>();
        selectionDetails = FindObjectOfType<SelectionDetails>();
        selectors = GetComponents<ISelector>();
    }

    protected override void DoActivation()
    {
        CurrentSelection = null;
        foreach (var selector in selectors)
            (selector as MonoBehaviour).enabled = true;
    }

    protected override void DoDeactivation()
    {
        CurrentSelection = null;
        foreach (var selector in selectors)
            (selector as MonoBehaviour).enabled = false;
    }
}
