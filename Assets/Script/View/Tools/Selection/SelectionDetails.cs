﻿using IfcEditorTypes;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Fills the Selection Detail Window with information on new selections.
/// </summary>
public class SelectionDetails : MonoBehaviour
{
    [SerializeField]
    private GameObject selectionDetailArea, detailPrefab, detailHolder;
    [SerializeField]
    private TMPro.TextMeshProUGUI detailHeader;

    private SelectTool selectTool;
    private IfcRepository ifcRepository;

    // Start is called before the first frame update
    void Start()
    {
        selectTool = FindObjectOfType<SelectTool>();
        ifcRepository = IfcRepository.GetIfcRepository();
    }

    public void UpdateSelectionDetails(GameObject newSelection)
    {
        //Clear previous Selection Details
        for (int i = detailHolder.transform.childCount - 1; i >= 0; i--)
        {
            DestroyImmediate(detailHolder.transform.GetChild(i).gameObject);
        }

        //Handle Deselection
        if (newSelection == null)
        {
            selectionDetailArea.SetActive(false);
            detailHeader.text = "Selection Details";
            return;
        };

        selectionDetailArea.SetActive(true);
        IfcElement info = ifcRepository.GetIfcElementOfGameObject(newSelection);

        if (info == null)
        {
            detailHeader.text = "No information available.";
            return;
        }

        detailHeader.text = info.elementName;

        InstantiateProperties(info, info.properties.Where(prop => prop.isActiveGlobal));
        
        Canvas.ForceUpdateCanvases();
        AdjustHeightOfHolder();
        Invoke("AdjustHeightOfHolder", 1f); // Hack: TextMeshPro doesnt update its height for wrapped words...
    }

    private void InstantiateProperties(IfcElement parent, IEnumerable<IfcProperty> props)
    {
        foreach (var property in props)
        {
            var detail = Instantiate(detailPrefab, detailHolder.transform);
            var textObj = detail.GetComponentInChildren<TMPro.TextMeshProUGUI>();
            var component = detail.GetComponent<SelectionDetailElement>();
            component.Initialize(parent, property, this);
            textObj.text = $"<b>{property.name}: </b> \n";
            detail.name = property.name;
            property.attributes.ForEach(prop => textObj.text += prop + "\n");
        };
    }

    public void ToggleAll(bool state)
    {
        foreach(var selectionDetail in detailHolder.GetComponentsInChildren<SelectionDetailElement>())
        {
            selectionDetail.SetState(state);
        }
    }

    //Called when a visibility Toggle of the Selection Detail Element is changed.
    internal void Refresh()
    {
        UpdateSelectionDetails(selectTool.CurrentSelection);
    }

    private void AdjustHeightOfHolder()
    {
        float height = 10;
        foreach (Transform detail in detailHolder.transform)
        {
            var elemRect = detail.GetComponent<RectTransform>();
            var text = detail.GetChild(0).GetComponent<TextMeshProUGUI>();
            var textSize = text.GetRenderedValues();
            elemRect.sizeDelta = new Vector2(textSize.x, textSize.y + 5);
            height += elemRect.sizeDelta.y + 5; // + Spacing
        }
        var detailRect = detailHolder.GetComponent<RectTransform>();
        detailRect.sizeDelta = new Vector2(detailRect.sizeDelta.x, height);
    }
}
