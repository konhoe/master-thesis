﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// An abstract Tool, handles activation and deactivation of the associated User Interface.
/// </summary>
public abstract class Tool : MonoBehaviour, ITool
{
    [SerializeField]
    private GameObject UI;

    public void Activate() {
        UI.SetActive(true);
        DoActivation();
    }

    public void Toggle()
    {
        if (UI.activeInHierarchy)
        {
            Deactivate();
        }
        else
        {
            Activate();
        }
    }

    /// <summary>
    /// Can be overriden in a Tool to add custom activation logic.
    /// </summary>
    protected abstract void DoActivation();

    public void Deactivate()
    {
        UI.SetActive(false);
        DoDeactivation();

    }

    /// <summary>
    /// Can be overriden in a Tool to add custom deactivation logic eg clean up.
    /// </summary>
    protected abstract void DoDeactivation();



}
