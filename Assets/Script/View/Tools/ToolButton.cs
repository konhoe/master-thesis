﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// UI button for a specific Tool. 
/// </summary>
[RequireComponent(typeof (Button))]
[ExecuteInEditMode]
public class ToolButton : MonoBehaviour
{
    [SerializeField]
    private Tool tool;

    private UIManager uiManager;
    private Button btn;

    public Tool Tool { get => tool; }
    public Button Btn { get => btn;  }

    // Start is called before the first frame update
    void Awake()
    {
        btn = GetComponent<Button>();    
        uiManager = FindObjectOfType<UIManager>();
        Btn.onClick.AddListener(ActivateTool);
    }

    private void ActivateTool()
    {
        uiManager.ActivateToolButton(this);
    }
}
