﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Handles rotation, translation and scaling of a loaded IFC Object.
/// </summary>
public class TransformTool : Tool {

    [SerializeField]
    private Vector3Input position, rotation, scale;
    [SerializeField]
    private Transform targetSelectionHolder;
    [SerializeField]
    private GameObject targetSelectionPrefab;

    private ToggleGroup toggleGroup;
    private List<Transform> transformTargets = new List<Transform>();
    private List<Toggle> toggles = new List<Toggle>();
    private Transform transformTarget;


    //TODO: Specify one loaded Object to Transform
    protected override void DoActivation()
    {
        this.enabled = true;
        transformTargets = new List<Transform>();
        toggleGroup = targetSelectionHolder.GetComponent<ToggleGroup>();
        FindObjectOfType<LoaderManager>().loadedObjects.ForEach(o =>
        {
            transformTargets.Add(o.transform.parent);
            var newToggle = Instantiate(targetSelectionPrefab, targetSelectionHolder).GetComponent<Toggle>();
            newToggle.group = toggleGroup;
            newToggle.onValueChanged.AddListener(UpdateActiveTarget);
            toggles.Add(newToggle);

            newToggle.GetComponentInChildren<TMPro.TMP_Text>().text = o.transform.parent.name;
        });
        if (toggles.Count > 0)
            toggles[0].isOn = true;
    }

    private void UpdateActiveTarget(bool arg0)
    {
        var active = toggleGroup.ActiveToggles().First();
        transformTarget = transformTargets[toggles.IndexOf(active)];

        position.SetVector(transformTarget.localPosition);
        rotation.SetVector(transformTarget.localEulerAngles);
        scale.SetVector(transformTarget.localScale);
    }

    protected override void DoDeactivation()
    {
        this.enabled = false;
        transformTargets = new List<Transform>();
        toggles.ForEach(t =>
        {
            toggleGroup.UnregisterToggle(t);
            Destroy(t.gameObject);
        });
        toggles = new List<Toggle>();
        transformTarget = null;
    }


    private void UpdateTarget()
    {
        transformTarget.localPosition = position.GetVector();
        transformTarget.localEulerAngles = rotation.GetVector();
        transformTarget.localScale = scale.GetVector();
    }

    private void Awake()
    {
        position.valuesChanged.AddListener(() => UpdateTarget());
        rotation.valuesChanged.AddListener(() => UpdateTarget());
        scale.valuesChanged.AddListener(() => UpdateTarget());
    }


}
