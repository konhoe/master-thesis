﻿using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Changes the UI based on the selected tool. 
/// </summary>
public class UIManager : MonoBehaviour
{
    [SerializeField]
    private Color activeColor, inactiveColor;

    [SerializeField]
    private List<ToolButton> toolButtons;

    internal void ActivateToolButton(ToolButton toolButton)
    {
        deactivateButtons();
        SetButtonColor(toolButton, activeColor);
        toolButton.Tool.Activate();
    }

    private void deactivateButtons()
    {
        toolButtons.ForEach((b) =>
        {
            SetButtonColor(b, inactiveColor);
            b.Tool.Deactivate();
        });
    }

    private void SetButtonColor(ToolButton b, Color c)
    {
        b.transform.GetChild(0).GetComponent<Image>().color = c;
    }

    private void Start()
    {
        toolButtons = FindObjectsOfType<ToolButton>().ToList();
    }

}
