﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

/// <summary>
/// Displays Errors to the User as an Error Popup.
/// </summary>
public class ErrorHandler : MonoBehaviour
{
    [SerializeField]
    private GameObject errorPopup;

    private TMP_Text errorText;

    public void Awake()
    {
        errorText = errorPopup.GetComponentInChildren<TMP_Text>();
    }

    public void ShowError(string message)
    {
        errorText.text = message;
        errorPopup.SetActive(true);
    }
}
