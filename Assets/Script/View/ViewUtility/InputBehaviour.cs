﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;


/// <summary>
/// Improve Usability for UI Number Inputs: 
/// Tab Navigation(https://forum.unity.com/threads/tab-between-input-fields.263779/)
/// Dragging Cursor to change Value
/// </summary>
[RequireComponent(typeof(InputField))]
public class InputBehaviour : MonoBehaviour {

    public float dragValue = 0.1f;

    EventSystem system;
    InputField baseInput;

    private void Awake()
    {
        CultureInfo.DefaultThreadCurrentCulture = CultureInfo.InvariantCulture;
        CultureInfo.DefaultThreadCurrentUICulture = CultureInfo.InvariantCulture;
    }

    void Start()
    {
        system = EventSystem.current;// EventSystemManager.currentSystem;
        baseInput = this.GetComponent<InputField>();

        EventTrigger trigger = GetComponent<EventTrigger>();
        EventTrigger.Entry entry = new EventTrigger.Entry();
        entry.eventID = EventTriggerType.Drag;
        entry.callback.AddListener((data) => { OnDrag((PointerEventData)data); });
        trigger.triggers.Add(entry);

    }

    private void OnDrag(PointerEventData data)
    {
        if (baseInput.text == "")
            baseInput.text = "0.0";

        float currentNumber = float.Parse(baseInput.text);

        float newNumber = currentNumber;
        
        if(data.delta.x > 0)
            newNumber = currentNumber + dragValue*data.delta.x;
        else
            newNumber = currentNumber + dragValue * data.delta.x;
        newNumber = (float)System.Math.Round(newNumber, 2);
        baseInput.text = newNumber.ToString();
    }

    void Update()
    {
        if (system.currentSelectedGameObject == this.gameObject && Input.GetKeyDown(KeyCode.Tab))
        {
            Selectable next;

            if(Input.GetKey(KeyCode.LeftShift) || Input.GetKey(KeyCode.RightShift))
                next = system.currentSelectedGameObject.GetComponent<Selectable>().FindSelectableOnLeft();
            else
                next = system.currentSelectedGameObject.GetComponent<Selectable>().FindSelectableOnRight();

            if (next != null)
            {
                InputField inputfield = next.GetComponent<InputField>();
                if (inputfield != null)
                    inputfield.OnPointerClick(new PointerEventData(system));

                system.SetSelectedGameObject(next.gameObject, new BaseEventData(system));
            }

        }
    }
}
