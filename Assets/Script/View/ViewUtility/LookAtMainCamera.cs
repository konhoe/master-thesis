﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Rotates transform towards Main Camera.
/// </summary>
public class LookAtMainCamera : MonoBehaviour
{
    [SerializeField] private bool x = true;
    [SerializeField] private bool y = true;
    [SerializeField] private bool z = true;

    private Transform mainCameraTransform;

    private void Start()
    {
        mainCameraTransform = Camera.main.transform;
    }

    void Update()
    {
        Vector3 look = mainCameraTransform.position - transform.position;
        if (x)
            look.x = transform.position.x;
        if (y)
            look.y = transform.position.y;
        if (z)
            look.z = transform.position.z;

        transform.rotation = Quaternion.LookRotation(look);
    }
}
