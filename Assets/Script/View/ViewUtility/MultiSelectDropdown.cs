﻿using System;
using System.Collections;
using System.Linq;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using FluentFTP;
using UnityEngine.Events;

/// <summary>
/// Manages a MultiSelect Dropdown.
/// </summary>
public class MultiSelectDropdown : MonoBehaviour
{
    [SerializeField]
    private GameObject holder, itemPrefab, blockerPrefab;
    [SerializeField]
    private TMP_Text currentSelection;

    public UnityEvent onValueChanged;
    private Dictionary<string, bool> options = new Dictionary<string, bool>();
    private float itemHeight = 0;
    private RectTransform holderRect;
    private GameObject activeBlocker;

    public Dictionary<string, bool> Options { get => new Dictionary<string, bool>(options); }

    /// <summary>
    /// Overwrites current options with the passed options.
    /// </summary>
    /// 
    /// <param name="newOptions">New options to show.</param>
    public void UpdateDropdown(Dictionary<string, bool> newOptions)
    {
        options = new Dictionary<string, bool>(newOptions);
        UpdateDropdownItems();
        UpdateDisplayText();
        onValueChanged.Invoke();
    }

    private void UpdateDropdownItems()
    {
        foreach(Transform t in holder.transform)
            Destroy(t.gameObject);
        foreach (KeyValuePair<string, bool> entry in options)
        {
            var uiItem = Instantiate(itemPrefab, holder.transform);
            var toggle = uiItem.GetComponentInChildren<Toggle>();
            var title = uiItem.GetComponentInChildren<TMP_Text>();

            title.text = entry.Key;
            toggle.isOn = entry.Value;
            toggle.onValueChanged.AddListener((v) =>
            {
                options[entry.Key] = v;
                UpdateDisplayText();
                onValueChanged.Invoke();
            });
        }
        Canvas.ForceUpdateCanvases();
    }

    private void UpdateDisplayText()
    {
        var newDisplayText = options.Aggregate("", (total, e) =>
        {
            return e.Value ? total + e.Key + ", " : total;
        });
        if (newDisplayText.Length > 2)
            newDisplayText = newDisplayText.Remove(newDisplayText.Length - 2);
        else
            newDisplayText = "No Button";
        currentSelection.text = newDisplayText;
        Canvas.ForceUpdateCanvases();
    }

    public void ToggleDropdown()
    {
        var isOpen = holder.activeInHierarchy;

        if (isOpen)
        {
            Destroy(activeBlocker);
            holder.SetActive(false);
        }
        else
        {
            //Add the invisible Blocker to close the dropdown on Outside Click
            activeBlocker = Instantiate(blockerPrefab, GetComponentInParent<Canvas>().transform);
            activeBlocker.GetComponent<Button>().onClick.AddListener(ToggleDropdown);

            holder.SetActive(true);
        }
        Canvas.ForceUpdateCanvases();
    }
}