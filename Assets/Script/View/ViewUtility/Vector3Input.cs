﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

/// <summary>
/// Common Monobehaviour for 3 Input fields for editing a Vector3.
/// </summary>
public class Vector3Input : MonoBehaviour {

    public InputField x;
    public InputField y;
    public InputField z;

    [HideInInspector]
    public UnityEvent valuesChanged;

    private void Awake()
    {
        x.onValueChanged.AddListener(delegate { valuesChanged.Invoke(); });
        y.onValueChanged.AddListener(delegate { valuesChanged.Invoke(); });
        z.onValueChanged.AddListener(delegate { valuesChanged.Invoke(); });
    }

    public void SetVector(Vector3 input)
    {
        x.SetTextWithoutNotify(""+ Math.Round(input.x, 5));
        y.SetTextWithoutNotify(""+ Math.Round(input.y, 5));
        z.SetTextWithoutNotify(""+ Math.Round(input.z, 5));
    }

    public Vector3 GetVector()
    {
        float xVal, yVal, zVal;
        float.TryParse(x.text, out xVal);
        float.TryParse(y.text, out yVal);
        float.TryParse(z.text, out zVal);

        return new Vector3(xVal, yVal, zVal);
    }

    

}
