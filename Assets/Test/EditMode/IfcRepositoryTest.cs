﻿using System.Collections;
using System.Collections.Generic;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;

namespace Tests
{
    public class IfcRepositoryTest
    {
        [Test]
        public void IfcRepositorySimple()
        {
            IfcRepository rep = IfcRepository.GetIfcRepository();
            Assert.AreEqual(0, rep.loadedXmls.Count);
        }
    }
}
