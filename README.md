# IFC Editor - Masters thesis

A Unity 3D desktop Application that allows annotating IFC files with comments and media files for the use in educational settings. The resulting projects can then be exported to an FTP Server where an augmented reality android app ( [IFC AR - Masters thesis](https://gitlab.com/konhoe/master-thesis-mobile) ) can access them.

 
## Structure

Scripts are located under [Assets/Scripts Folder](Assets/Script) and divided into Repositories (Import & Export Logic, File Access and Server Access, ...) and View Components (Unity Scripts that handle Buttons, Windows, Textfields, Spawning of 3D models, ...).

## Configuration

The [ConfigLoader](Assets/Script/Repository/ConfigLoader.cs) expects a config.json in the Assets/StreamingAssets folder in the following format:

```
{
  "ifcConvertPath": "./00-Modelle/IfcConvert_v6.exe",
  "outputPath": "./ConversionResults",
  "defaultFtpUrl": "URL",
  "defaultFtpUsername": "USERNAME",
  "defaultFtpPassword": "PASSWORD"
}
```

## IFC & Project Loading

Different Repositories handle reading from IFC XML and JSON Files, the [XML Loader](Assets/Script/Repository/XmlLoader.cs) parses the IFC input file with [ifcconvert](http://ifcopenshell.org/ifcconvert) and stores a XML representation of it in the configurations outputPath for further access through the [IFC Repository](Assets/Script/Model/IfcRepository.cs). The [TrilibLoader](Assets/Script/Repository/TrilibLoader.cs) spawns the IFC Model in the scene and applies some visual defaults.
The [FTP Repository](Assets/Script/Repository/FTPRepository.cs) manages the FTP connection and stores downloaded projects, which are opened through the [JSON IFC Repository](Assets/Script/Repository/JsonIfcRepository.cs).

To be continued...
